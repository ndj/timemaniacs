<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),

            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),

            new FOS\UserBundle\FOSUserBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\Bundle\MarkdownBundle\KnpMarkdownBundle(),
            //new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            
            new Liip\ImagineBundle\LiipImagineBundle(),
            new PK\MarkdownifyBundle\PKMarkdownifyBundle(),
            new ADesigns\CalendarBundle\ADesignsCalendarBundle(),
            new Mylen\JQueryFileUploadBundle\JQueryFileUploadBundle(),
            new Braincrafted\BootstrapBundle\BraincraftedBootstrapBundle(),
            
            new Timemaniacs\TagBundle\TimemaniacsTagBundle(),
            new Timemaniacs\UserBundle\TimemaniacsUserBundle(),
            new Timemaniacs\NodeBundle\TimemaniacsNodeBundle(),
            new Timemaniacs\AliasBundle\TimemaniacsAliasBundle(),
            new Timemaniacs\ChartsBundle\TimemaniacsChartsBundle(),
            new Timemaniacs\ProjectBundle\TimemaniacsProjectBundle(),
            //new Timemaniacs\CompanyBundle\TimemaniacsCompanyBundle(),
            new Timemaniacs\ServiceBundle\TimemaniacsServiceBundle(),
            new Timemaniacs\CalendarBundle\TimemaniacsCalendarBundle(),
            new Timemaniacs\CustomerBundle\TimemaniacsCustomerBundle(),
            //new Timemaniacs\ActivityTimeBundle\TimemaniacsActivityTimeBundle(),
            
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            //$bundles[] = new Acme\DemoBundle\AcmeDemoBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
