<?php
// src/Acme/DemoBundle/Menu/Builder.php
namespace Timemaniacs\CustomerBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $subMenu = $menu->addChild('Your Costumers');
        $subMenu->addChild('Your Costumers', array('route' => 'customer'));
        $subMenu->addChild('New Costumer', array('route' => 'customer_new'));

        return $menu;
    }
}
