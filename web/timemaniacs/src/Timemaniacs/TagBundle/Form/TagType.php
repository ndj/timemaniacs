<?php

namespace Timemaniacs\TagBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TagType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            //->add('created')
            //->add('updated')
            ->add('company')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Timemaniacs\TagBundle\Entity\Tag'
        ));
    }

    public function getName()
    {
        return 'timemaniacs_tagbundle_tagtype';
    }
}
