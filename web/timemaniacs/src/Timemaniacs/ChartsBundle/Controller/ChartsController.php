<?php

namespace Timemaniacs\ChartsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\SecurityExtraBundle\Annotation\Secure;

class ChartsController extends Controller
{
    /**
     * @Route("/charts")
     * @Template()
     */
    public function indexAction()
    {
        return array('name' => 'charts');
    }
}
