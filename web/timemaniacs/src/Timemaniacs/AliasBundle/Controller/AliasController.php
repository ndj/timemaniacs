<?php

namespace Timemaniacs\AliasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Timemaniacs\AliasBundle\Entity\Alias;
use Timemaniacs\AliasBundle\Form\AliasType;

/**
 * Alias controller.
 *
 * @Route("/alias")
 */
class AliasController extends Controller
{
    /**
     * Lists all Alias entities.
     *
     * @Route("/", name="alias")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TimemaniacsAliasBundle:Alias')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Alias entity.
     *
     * @Route("/", name="alias_create")
     * @Method("POST")
     * @Template("TimemaniacsAliasBundle:Alias:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Alias();
        $form = $this->createForm(new AliasType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('alias_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Alias entity.
     *
     * @Route("/new", name="alias_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Alias();
        $form   = $this->createForm(new AliasType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Alias entity.
     *
     * @Route("/{id}", name="alias_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsAliasBundle:Alias')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Alias entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Alias entity.
     *
     * @Route("/{id}/edit", name="alias_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsAliasBundle:Alias')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Alias entity.');
        }

        $editForm = $this->createForm(new AliasType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Alias entity.
     *
     * @Route("/{id}", name="alias_update")
     * @Method("PUT")
     * @Template("TimemaniacsAliasBundle:Alias:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsAliasBundle:Alias')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Alias entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new AliasType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('alias_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Alias entity.
     *
     * @Route("/{id}", name="alias_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TimemaniacsAliasBundle:Alias')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Alias entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('alias'));
    }

    /**
     * Creates a form to delete a Alias entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
