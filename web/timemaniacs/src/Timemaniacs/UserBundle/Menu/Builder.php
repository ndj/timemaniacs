<?php
// src/Acme/DemoBundle/Menu/Builder.php
namespace Timemaniacs\ProjectBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Profile');
        $menu['Profile']->addChild('Profile', array('route' => 'profile'));
        $menu['Profile']->addChild('Profile', array('route' => 'profile_edit'));
        $menu['Profile']->addChild('Company', array('route' => 'company'));
        $menu['Profile']->addChild('New Company', array('route' => 'company_new'));
        
        
        return $menu;
    }
}
