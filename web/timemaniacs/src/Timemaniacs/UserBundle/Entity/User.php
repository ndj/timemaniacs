<?php

namespace Timemaniacs\UserBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="\Timemaniacs\UserBundle\Entity\Company", inversedBy="users", cascade={"persist"})
     * @ORM\JoinTable(name="user_company")
     */
    private $company;

    /**
     * @ORM\OneToMany(targetEntity="\Timemaniacs\ProjectBundle\Entity\Activity", mappedBy="user")
     */
    private $activities;
    
    
    public function __construct()
    {
        parent::__construct();
        
        $this->company = new ArrayCollection();
        $this->activities = new ArrayCollection();
        // your own logic
    }
    
    public function __toString() {
        return $this->username;
    }

    /**
     * Set company
     *
     * @param \Timemaniacs\UserBundle\Entity\Company $company
     * @return User
     */
    public function setCompany(\Timemaniacs\UserBundle\Entity\Company $company = null)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return \Timemaniacs\UserBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    public function getActivities() {
        return $this->activities;
    }

    public function setActivities($activities) {
        $this->activities = $activities;
    }

        
    /**
     * Add company
     *
     * @param \Timemaniacs\UserBundle\Entity\Company $company
     * @return User
     */
    public function addCompany(\Timemaniacs\UserBundle\Entity\Company $company)
    {
        $this->company[] = $company;
    
        return $this;
    }

    /**
     * Remove company
     *
     * @param \Timemaniacs\UserBundle\Entity\Company $company
     */
    public function removeCompany(\Timemaniacs\UserBundle\Entity\Company $company)
    {
        $this->company->removeElement($company);
    }
}