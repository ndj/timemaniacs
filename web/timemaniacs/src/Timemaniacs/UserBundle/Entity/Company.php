<?php

namespace Timemaniacs\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="Timemaniacs\UserBundle\Entity\CompanyRepository")
 */
class Company
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="\Timemaniacs\UserBundle\Entity\User", mappedBy="company")
     */
    private $users;
    
    /**
     * @ORM\OneToMany(targetEntity="\Timemaniacs\ProjectBundle\Entity\Project", mappedBy="company")
     */
    private $projects;
    
    /**
     * @ORM\OneToMany(targetEntity="\Timemaniacs\ProjectBundle\Entity\Activity", mappedBy="company")
     */
    private $activities;

    /**
     * @ORM\OneToMany(targetEntity="\Timemaniacs\ProjectBundle\Entity\Story", mappedBy="company")
     */
    private $stories;
    
    /**
     * @ORM\Column(name="created", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $created;

    /**
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;

    public function __construct() {
        $this->users = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->activities = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set users
     *
     * @param \stdClass $users
     * @return Company
     */
    public function setUsers($users)
    {
        $this->users = $users;
    
        return $this;
    }

    /**
     * Get users
     *
     * @return \stdClass 
     */
    public function getUsers()
    {
        return $this->users;
    }

    
    public function getStories() {
        return $this->stories;
    }

    public function setStories($stories) {
        $this->stories = $stories;
    }
    
    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Company
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Company
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    
    public function __toString() {
        return $this->name;
    }

    /**
     * 
     * @return type
     */
    public function getActivities() {
        return $this->activities;
    }

    /**
     * 
     * @param type $activities
     */
    public function setActivities($activities) {
        $this->activities = $activities;
    }

    /**
     * 
     * @return type
     */
    public function getProjects() {
        return $this->projects;
    }

    /**
     * 
     * @param type $projects
     */
    public function setProjects($projects) {
        $this->projects = $projects;
    }
    
    /**
     * Add users
     *
     * @param \Timemaniacs\UserBundle\Entity\User $users
     * @return Company
     */
    public function addUser(\Timemaniacs\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;
    
        return $this;
    }

    /**
     * Remove users
     *
     * @param \Timemaniacs\UserBundle\Entity\User $users
     */
    public function removeUser(\Timemaniacs\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }
    
    
    public function existUser($users)
    {
        foreach($this->users as $user)
        {
            //var_dump($user->getCompany());die('hiers');
            if ( $user->getId() === $user->getId() )
                return true;
        }
        return false;
    }
}