<?php

namespace Timemaniacs\ProjectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ActivityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('totaltime')
            ->add('name')
            ->add('tags', null , array(
                'required' => FALSE 
            ))
            ->add('description')
            ->add('start', 'datetime', array( 'date_widget' => 'single_text', 'time_widget' => 'text'))
            ->add('end', 'datetime', array( 'date_widget' => 'single_text', 'time_widget' => 'text'))
            ->add('project')
            ->add('story')
            ->add('service')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Timemaniacs\ProjectBundle\Entity\Activity',
            'csrf_protection' => FALSE,
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class'      => 'Timemaniacs\ProjectBundle\Entity\Activity',
            'csrf_protection' => FALSE,
        );
    }

    public function getName()
    {
        return 'timemaniacs_projectbundle_activitytype';
    }
}
