<?php

namespace Timemaniacs\ProjectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ActivityTimeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('totalTime')
            //->add('activity')
            ->add('start', 'datetime', array( 'date_widget' => 'single_text', 'time_widget' => 'text'))
            ->add('end', 'datetime', array( 'date_widget' => 'single_text', 'time_widget' => 'text'))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Timemaniacs\ProjectBundle\Entity\ActivityTime'
        ));
    }

    public function getName()
    {
        return 'timemaniacs_projectbundle_activitytimetype';
    }
}
