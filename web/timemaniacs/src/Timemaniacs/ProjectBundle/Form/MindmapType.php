<?php

namespace Timemaniacs\ProjectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MindmapType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('map')
            ->add('created')
            ->add('updated')
            ->add('company')
            ->add('user')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Timemaniacs\ProjectBundle\Entity\Mindmap'
        ));
    }

    public function getName()
    {
        return 'timemaniacs_projectbundle_mindmaptype';
    }
}
