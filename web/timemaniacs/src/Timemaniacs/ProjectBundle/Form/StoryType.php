<?php

namespace Timemaniacs\ProjectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Timemaniacs\ProjectBundle\Form\ActivityType;

class StoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('activities', 'collection', array(
                    'type'          => new ActivityType(),
                    'allow_add'     => true,
                    'allow_delete'  => true,
                    'by_reference'  => false,
                )
            )
            ->add('description')
            ->add('project')
            ->add('company')
            ->add('sprint')
            ->add('deadline', 'datetime', array( 'date_widget' => 'single_text', 'time_widget' => 'text'))
            ->add('start', 'datetime', array( 'date_widget' => 'single_text', 'time_widget' => 'text'))
            ->add('end', 'datetime', array( 'date_widget' => 'single_text', 'time_widget' => 'text'))
            //->add('created')
            //->add('updated')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Timemaniacs\ProjectBundle\Entity\Story'
        ));
    }

    public function getName()
    {
        return 'timemaniacs_projectbundle_storytype';
    }
}
