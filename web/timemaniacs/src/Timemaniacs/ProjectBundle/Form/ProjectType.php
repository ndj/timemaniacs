<?php

namespace Timemaniacs\ProjectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProjectType extends AbstractType
{

    private $user;

    public function __construct($user) {
        $this->user = $user;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $uid = $this->user->getId();
        $builder
            ->add('name')

            ->add('tags')
            ->add('activities',null, array(
                'required' => false
            ))
            ->add('company', 'entity', array(
                'class' => 'TimemaniacsUserBundle:Company',
                'query_builder' => function($er) use ($uid) {

                  // show only the list of companies to this user
                  return $er->createQueryBuilder('c')
                            ->innerJoin('c.users', 'u')
                            ->andWhere('u.id = :id')
                            ->setParameter('id', $uid)
                            //->orderBy('u.username', 'ASC');
                          ;
                  },)
             )
             ->add('deadline', 'datetime', array( 'date_widget' => 'single_text', 'time_widget' => 'text'))
             ->add('totalBudgetTime')
             ->add('totalBudget')
             ->add('projectResourceAlert')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Timemaniacs\ProjectBundle\Entity\Project'
        ));
    }

    public function getName()
    {
        return 'timemaniacs_projectbundle_projecttype';
    }
}
