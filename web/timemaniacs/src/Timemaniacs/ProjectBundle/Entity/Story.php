<?php

namespace Timemaniacs\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Story
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Timemaniacs\ProjectBundle\Entity\StoryRepository")
 */
class Story
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="\Timemaniacs\ProjectBundle\Entity\Activity", mappedBy="story", cascade={"persist"})
     */
    private $activities;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     *
     * @var type 
     * 
     * @ORM\ManyToOne(targetEntity="\Timemaniacs\ProjectBundle\Entity\Project", inversedBy="stories") 
     */
    private $project;
    
    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="\Timemaniacs\UserBundle\Entity\Company", inversedBy="stories") 
     */
    private $company;

    /**
     * @var integer
     *
     * @ORM\Column(name="sprint", type="integer")
     */
    private $sprint;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime")
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime")
     */
    private $end;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deadline", type="datetime")
     */
    private $deadline;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * 
     */
    public function __construct() {
        $this->activities = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Story
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set activities
     *
     * @param integer $activities
     * @return Story
     */
    public function setActivities($activities)
    {
        $this->activities = $activities;
    
        return $this;
    }

    /**
     * Get activities
     *
     * @return integer 
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Story
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set company
     *
     * @param integer $company
     * @return Story
     */
    public function setCompany($company)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return integer 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set sprint
     *
     * @param integer $sprint
     * @return Story
     */
    public function setSprint($sprint)
    {
        $this->sprint = $sprint;
    
        return $this;
    }

    /**
     * Get sprint
     *
     * @return integer 
     */
    public function getSprint()
    {
        return $this->sprint;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return Story
     */
    public function setStart($start)
    {
        $this->start = $start;
    
        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return Story
     */
    public function setEnd($end)
    {
        $this->end = $end;
    
        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Story
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Story
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    
    public function getProject() {
        return $this->project;
    }

    public function setProject($project) {
        $this->project = $project;
    }

        
   /* public function addActivity( $activity)
    {
        $activity->addStory($this);

        $this->activities->add($activity);
    }*/
    
    public function addActivity($activity)
    {
        $activity->addStory($this);
        $this->activities->add($activity);
    }

    public function removeActivity($activity)
    {
        $this->activities->removeElement($activity);
    }

    public function getDeadline() {
        return $this->deadline;
    }

    public function setDeadline(\DateTime $deadline) {
        $this->deadline = $deadline;
    }

    /**
     * 
     * @return type
     */
    public function __toString() {
        return $this->name;
    }
    
}
