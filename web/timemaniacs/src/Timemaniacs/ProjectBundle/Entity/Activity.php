<?php

namespace Timemaniacs\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Activity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Timemaniacs\ProjectBundle\Entity\ActivityRepository")
 */
class Activity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="totaltime", type="integer", nullable=true)
     */
    private $totaltime;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var text
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var \stdClass
     * @ORM\ManyToMany(targetEntity="\Timemaniacs\TagBundle\Entity\Tag", inversedBy="activities", cascade={"persist"})
     * @ORM\JoinTable(name="tags_activities")
     * ####ORM\Column(name="tags", type="object", nullable=true)
     */
    private $tags;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;
    
    /**
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     */
    private $start;
    
    /**
     *
     * @ORM\Column(name="end", type="datetime", nullable=true)
     */
    private $end;

    /**
     *
     * @ORM\Column(name="running", type="boolean")
     */
    private $running = 0;
    
    /**
     * @ORM\ManyToOne(targetEntity="\Timemaniacs\ProjectBundle\Entity\Project", inversedBy="activities")
     * @ORM\JoinColumn(name="project", referencedColumnName="id")
     * @ORM\JoinColumn(nullable=true)
     */
    private $project;
    
    /**
     * @ORM\OneToMany(targetEntity="\Timemaniacs\ProjectBundle\Entity\ActivityTime", mappedBy="activity", cascade={"persist"})
     * @ORM\JoinColumn(name="activity", referencedColumnName="id")
     */
    private $ActivityTimes;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="\Timemaniacs\UserBundle\Entity\Company", inversedBy="activities", cascade={"persist"}) 
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="\Timemaniacs\ServiceBundle\Entity\Service", inversedBy="activities", cascade={"persist"}) 
     */
    private $service;
    
    /**
     * @var \stdClass
     * @ORM\ManyToOne(targetEntity="\Timemaniacs\ProjectBundle\Entity\Story", inversedBy="activities", cascade={"persist"}) 
     */
    private $story;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="\Timemaniacs\UserBundle\Entity\User", inversedBy="activities", cascade={"persist"}) 
     */    
    private $user;

    /**
     *
     * @ORM\Column(name="estimated_days", type="integer", nullable=true)
     */
    private $estimatedDays;
    
    /**
     * @ORM\OneToMany(targetEntity="\Timemaniacs\ProjectBundle\Entity\Activity", mappedBy="parent")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="\Timemaniacs\ProjectBundle\Entity\Activity", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * @ORM\JoinColumn(nullable=true)
     */
    private $parent;

    /**
     * 
     */
    public function __construct() {
        $this->description = 'Click to edit';
        $this->children = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set totaltime
     *
     * @param integer $totaltime
     * @return Activity
     */
    public function setTotaltime($totaltime)
    {
        $this->totaltime = $totaltime;
    
        return $this;
    }

    /**
     * Get totaltime
     *
     * @return integer 
     */
    public function getTotaltime()
    {
        return $this->totaltime;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Activity
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tags
     *
     * @param \stdClass $tags
     * @return Activity
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    
        return $this;
    }

    /**
     * Get tags
     *
     * @return \stdClass 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Activity
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Activity
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    public function getStart() {
        return $this->start;
    }

    public function setStart($start) {
        $this->start = $start;
    }

    public function getEnd() {
        return $this->end;
    }

    public function setEnd($end) {
        $this->end = $end;
    }

    /**
     * Set project
     *
     * @param \stdClass $project
     * @return Activity
     */
    public function setProject($project)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \stdClass 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * 
     * @return type
     */
    public function getActivityTimes() {
        return $this->ActivityTimes;
    }

    /**
     * 
     * @param type $ActivityTimes
     */
    public function setActivityTimes($ActivityTimes) {
        $this->ActivityTimes = $ActivityTimes;
    }

    public function getRunning() {
        return $this->running;
    }

    public function setRunning($running) {
        $this->running = $running;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getUser() {
        return $this->user;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function getCompany() {
        return $this->company;
    }

    public function setCompany($company) {
        $this->company = $company;
    }

    public function getEstimatedDays() {
        return $this->estimatedDays;
    }

    public function setEstimatedDays($estimatedDays) {
        $this->estimatedDays = $estimatedDays;
    }

    public function getService() {
        return $this->service;
    }

    public function setService($service) {
        $this->service = $service;
    }

    public function getStory() {
        return $this->story;
    }

    public function setStory($story) {
        $this->story = $story;
    }

    public function getParent() {
        return $this->parent;
    }

    public function setParent($parent) {
        $this->parent = $parent;
    }

    public function getChildren() {
        return $this->children;
    }

    public function setChildren($children) {
        $this->children = $children;
    }

        
    
    public function addStory($story)
    {
        //if (!$this->story->contains($story)) {
        $this->story = $story;
        // }
    }        
    /**
     * 
     * @param type $diffTime
     * @return type
     */
    public function getTotaltimetoArray($diffTime){
        $years   = floor($diffTime / (365*60*60*24)); 
        $months  = floor(($diffTime - $years * 365*60*60*24) / (30*60*60*24)); 
        $days    = floor(($diffTime - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

        $hours   = floor(($diffTime - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60)); 

        $minuts  = floor(($diffTime - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60); 

        $seconds = floor(($diffTime - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60)); 

        $return = array(
            'years' => $years,
            'months' => $months,
            'days' => $days,
            'hours' => $hours,
            'months' => $months,
            'seconds' => $seconds
        );
        return $return;
    }
    
    public function __toString() {
        return $this->name;
    }
}
