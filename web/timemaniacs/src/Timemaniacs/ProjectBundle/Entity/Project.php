<?php

namespace Timemaniacs\ProjectBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="Timemaniacs\ProjectBundle\Entity\ProjectRepository")
 */
class Project
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="\Timemaniacs\UserBundle\Entity\Company", inversedBy="projects")
     * @ORM\JoinColumn(name="company", referencedColumnName="id")
     */
    private $company;

    /**
     *
     * @ORM\Column(name="description", type="text",nullable=true)
     */
    private $description;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="tags", type="object")
     */
    private $tags;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     *
     * @ORM\Column(name="start", type="datetime", nullable=true)
     */
    private $start;

    /**
     *
     * @ORM\Column(name="end", type="datetime", nullable=true)
     */
    private $end;


    /**
     * @ORM\Column(name="deadline", type="datetime", nullable=true)
     */
    private $deadline;
    
    /**
     * @ORM\Column(name="total_budget_time", type="integer", nullable=true)
     */
    private $totalBudgetTime;
    
    /**
     * @ORM\Column(name="total_budget", type="integer", nullable=true)
     */
    private $totalBudget;

    /**
     *
     * @ORM\Column(name="estimate", type="integer", nullable=true)
     */
    private $estimate;
    
    /**
     * @ORM\Column(name="project_resource_alert", type="integer", nullable=true)
     */
    private $projectResourceAlert;
    
    /**
     * @ORM\OneToMany(targetEntity="\Timemaniacs\ProjectBundle\Entity\Activity", mappedBy="project", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $activities;

    /**
     * @ORM\OneToMany(targetEntity="\Timemaniacs\ProjectBundle\Entity\Story", mappedBy="project", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */    
    private $stories;

    /**
     * @ORM\OneToMany(targetEntity="\Timemaniacs\NodeBundle\Entity\Wiki", mappedBy="project", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $wikimedia;
    
    /**
     *
     */
    public function __construct() {
        $this->activities = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tags
     *
     * @param \stdClass $tags
     * @return Project
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    
        return $this;
    }

    /**
     * Get tags
     *
     * @return \stdClass 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Project
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Project
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }
    
    public function getCompany() {
        return $this->company;
    }

    public function setCompany($company) {
        $this->company = $company;
    }

    public function getActivities() {
        return $this->activities;
    }

    public function setActivities($activities) {
        $this->activities = $activities;
    }
    
    public function getDeadline() {
        return $this->deadline;
    }

    public function setDeadline($deadline) {
        $this->deadline = $deadline;
    }

    public function getTotalBudgetTime() {
        return $this->totalBudgetTime;
    }

    public function getTotalBudget() {
        return $this->totalBudget;
    }

    public function setTotalBudget($totalBudget) {
        $this->totalBudget = $totalBudget;
    }
  
    public function setTotalBudgetTime($totalBudgetTime) {
        $this->totalBudgetTime = $totalBudgetTime;
    }

    public function getProjectResourceAlert() {
        return $this->projectResourceAlert;
    }

    public function setProjectResourceAlert($projectResourceAlert) {
        $this->projectResourceAlert = $projectResourceAlert;
    }
    
    public function getEstimate() {
        return $this->estimate;
    }

    public function setEstimate($estimate) {
        $this->estimate = $estimate;
    }

    public function getStart() {
        return $this->start;
    }

    public function setStart($start) {
        $this->start = $start;
    }

    public function getEnd() {
        return $this->end;
    }

    public function setEnd($end) {
        $this->end = $end;
    }
    
    public function getStories() {
        return $this->stories;
    }

    public function setStories($stories) {
        $this->stories = $stories;
    }

    
    public function getWikimedia() {
        return $this->wikimedia;
    }

    public function setWikimedia($wikimedia) {
        $this->wikimedia = $wikimedia;
    }

        
    public function __toString() {
        return $this->name;
    }
    
    
    
}
