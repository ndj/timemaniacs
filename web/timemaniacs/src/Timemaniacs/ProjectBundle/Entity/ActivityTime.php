<?php

namespace Timemaniacs\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ActivityTime
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Timemaniacs\ProjectBundle\Entity\ActivityTimeRepository")
 */
class ActivityTime
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(name="total_time", type="integer", nullable=true)
     */
    private $totalTime;

    /**
     * @ORM\ManyToOne(targetEntity="\Timemaniacs\ProjectBundle\Entity\Activity", inversedBy="ActivityTimes")
     * @ORM\JoinColumn(name="activity", referencedColumnName="id")
     */
    private $activity;

    /**
     * @ORM\Column(name="billable", type="boolean", nullable=true)
     */
    private $billable = 1;
    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\Column(name="start", type="datetime", nullable=true)
     */
    private $start;

    /**
     * @ORM\Column(name="end", type="datetime", nullable=true)
     */
    private $end;

    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set totalTime
     *
     * @param integer $totalTime
     * @return ActivityTime
     */
    public function setTotalTime($totalTime)
    {
        $this->totalTime = $totalTime;
    
        return $this;
    }

    /**
     * Get totalTime
     *
     * @return integer 
     */
    public function getTotalTime()
    {
        return $this->totalTime;
    }

    /**
     * Set activity
     *
     * @param integer $activity
     * @return ActivityTime
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;
    
        return $this;
    }

    /**
     * Get activity
     *
     * @return integer 
     */
    public function getActivity()
    {
        return $this->activity;
    }

    public function getBillable() {
        return $this->billable;
    }

    public function setBillable($billable) {
        $this->billable = $billable;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return ActivityTime
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated() {
        return $this->updated;
    }

    public function setUpdated($updated) {
        $this->updated = $updated;
    }

    public function getStart() {
        return $this->start;
    }

    public function setStart($start) {
        $this->start = $start;
    }

        
    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return ActivityTime
     */
    public function setEnd($end)
    {
        $this->end = $end;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }
}
