    
    $(function (){
    
        $('form.navbar-search input').click(function () {
            $(this).animate({
                width: '400px'
            }, 1000 );
        });
        
        $('form.navbar-search input').blur(function () {
            var value = $(this).val();
          //  if (value === ''){
                $(this).animate({
                    width: '250px'
                }, 1000 );
          //  }
        });

        $('form.navbar-search input').keypress(function(e) {
            if(e.which === 13) {
                addOverlay();
                $('form.navbar-search').submit(function (e) {
                    e.preventDefault();
                });
                var formAction = $('form.navbar-search').attr('action');

                $.ajax({
                    type: "POST",
                    url: Routing.generate('activity_create_ajax'),
                    data: { name: $('form.navbar-search input').val() },
                    error: function () {
                        alert('not working search');
                        $('#overlay').remove();
                    },
                    success: function (ajax_json_return){
                      // alert('hiers');
                      alert(ajax_json_return);
                      json_object = jQuery.parseJSON(ajax_json_return);
                      var projectString = '';
                      if (json_object.project !== false){
                          projectString = '<a href="' + Routing.generate('project_show', { id: 12 }) + '"><span class="label label-inverse">' + json_object.project + '</span></a>';
                      }
                      
                      
                      var newRecord = '<div class="accordion-group">\n\
            <div class="accordion-heading">\n\
                <div class="accordion-toggle block" data-toggle="collapse" data-parent="#accordion" href="#collapse' + json_object.id + '">\n\
                    <p class="lead pull-left">\n\
                        <span class="editable activity-id-' + json_object.id + '" title="Click to edit...">' + json_object.name + '</span>\n\
        ' +  projectString + '\n\
                    </p>\n\
                    <div class="btn-group pull-right">\n\
                        <a id="activity-timer-' + json_object.id + '" class="activity-timer btn stoped" href="/app_dev.php/activity/' + json_object.id + '/start">00:00:00</a>\n\
                        <a class="btn btn-actions" href="/app_dev.php/activity/' + json_object.id + '">Show</a>\n\
                        <a class="btn btn-actions" href="/app_dev.php/activity/' + json_object.id + '/edit">Edit</a>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
            <div id="collapse' + json_object.id + '" class="accordion-body collapse">\n\
              <div class="accordion-inner">\n\
                  <div class="activity-task-description editable-description activity-description-id-' + json_object.id + '" title="Click to edit...">Click to edit</div>\n\
                    <table class="table">\n\
                        <tbody>\n\
                            <tr class="border-none">\n\
                                <th>Todo create compleet ajax result</th>\n\
                                <table class="table">\n\
                    <tbody><tr class="border-none">\n\
                        <th>\n\
                            Total time\n\
                        </th>\n\
                        <th>\n\
                            From\n\
                        </th>\n\
                        <th>\n\
                            Untill\n\
                        </th>\n\
                        <th>\n\
                            Actions\n\
                        </th>\n\
                        <th>\n\
                            Actions\n\
                        </th>\n\
                    </tr>\n\
                                                <tr>\n\
                              <td>\n\
                                  \n\
                              </td>\n\
                              <td>\n\
                                  <strong>' + json_object.start + '</strong>\n\
                              </td>\n\
                              <td>\n\
                                  <strong>' + json_object.end + '</strong>\n\
                              </td>\n\
                              <td>\n\
\n\
                                <div data-toggle="buttons-radio" class="btn-group pull-right">\n\
                                    <a href="#" class="btn btn-success">Billable</a>\n\
                                    <a href="#" class="btn btn-danger">Not billable</a>\n\
                                </div>\n\
                               </td>\n\
                               <td>\n\
                                <div class="btn-group pull-right">\n\
                                    <a href="/app_dev.php/activitytime/' + json_object.id + '" class="btn">Show</a>\n\
                                    <a href="/app_dev.php/activitytime/' + json_object.id + '/edit" class="btn">Edit</a>\n\
                                    <a href="/app_dev.php/activitytime/' + json_object.id + '" class="btn btn-danger">Delete</a>\n\
                                </div>\n\
                              </td>\n\
                          </tr>\n\
                                        </tbody></table>\n\
                            </tr>\n\
                        </tbody></table>\n\
                \n\
                \n\
                \n\
                \n\
                  <form id="form-tag-' + json_object.id + '" class="form-tag" action="/app_dev.php/tag/ajax/add-remove" method="post">\n\
                      <div class="input-append">\n\
                            <input type="text" class="activity-tags span3" name="tags" value="" placeholder="Add[+] and remove[-] tags">\n\
                            <button class="timeslice-save-tags btn" title="Execute"><i class="icon-tags"></i></button>\n\
                      </div>\n\
                  </form>\n\
              </div>\n\
            </div>\n\
        </div>';
$('#accordion').prepend(newRecord);
$('#overlay').remove();
                    }
                });
                

                
                return false;
            }
        });


    $('#accordion .accordion-inner form.form-tag button.timeslice-save-tags.btn').click( function () {

        addOverlay();

        var formAction = $(this).parent('div').parent('form').attr('action');
        var activityAttrId = $(this).parent('div').parent('form').attr('id');

        var activityId = activityAttrId.replace('form-tag-','');
        var newTag = $('form#form-tag-' + activityId + ' input.activity-tags').val();

        var formAccordionPath = $('form#form-tag-' + activityId).parent('div.accordion-inner').parent('div.accordion-body').parent('div.accordion-group');

       $.ajax({
             type: "POST",
             url: formAction,
             data: { activity_id: activityId, tag: newTag },
             error: function () {
                 alert('not working home');
                 $('#overlay').remove();
             },
             success: function (msg){
                if (msg === 'added'){
                    formAccordionPath.find('p.lead').append('<span class="label">' + newTag.replace('+','') + '</span>');
                    $('form#form-tag-' + activityId + ' input.activity-tags').val('');
                }
                else if (msg === 'removed') {

                    formAccordionPath.find('p.lead span.label:contains("' + newTag.replace('-','').trim() + '")').remove();
                    $('form#form-tag-' + activityId + ' input.activity-tags').val(''); 
                }
                $('#overlay').remove();

             }
         });

        return false;
    });

});
    