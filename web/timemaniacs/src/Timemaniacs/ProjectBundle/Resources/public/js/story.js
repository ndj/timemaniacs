$(function () {
    $('.editable-story').editable(Routing.generate('story_ajax_edit') ,{
            indicator   : 'Saving...',
            method      : 'post',
            tooltip     : 'Click to edit...',
            style       : 'display: inline',
            height      : 22,
            submitdata  : function (value, settings, name){

            var currentId = $(this).attr('id');
            var storyId = currentId.replace('story-id-','');

            return {id: storyId};
        },
        callback : function(value, settings) {
            //$('.alert').alert();
            //alert('done');
           // $(this).parent('p.lead').parent('div.accordion-toggle').parent('.accordion-heading').parent('div.accordion-group').addClass('success');
        }
    });
    

    $('.editable-story-description').editable(Routing.generate('story_ajax_set_description'),{
            indicator : 'Saving...',
            type      : 'textarea',
            submit    : 'OK',
            cancel    : 'Cancel',
            method    : 'post',
            tooltip   : 'Click to edit...',
            style     : 'position: absolute',
            height    : 100,
            width     : 400,
            data      : function (value, settings) {
    
               /* alert(value);
                var reMarker = new reMarked();
                var markdown = reMarker.render(value);*/
        return value;
                //return markdown;
            },
            cssclass  : 'markdown',
            submitdata: function (value, settings, name){

                var currentClass = $(this).attr('id');
                var storyId = currentClass.replace('story-description-id-','');

                //alert('hiers nu' + name);
                return {id: storyId};
            },
        callback : function(value, settings) {
            //$(".alert").alert();
            //$(this).parent('p.lead').parent('div.accordion-toggle').parent('.accordion-heading').parent('div.accordion-group').addClass('success');
        }
    });

    
});