  $(document).ready(function() {
    
    //- activate running activities
    var timer = '';

    //- foreach timerbutton on the activities overview page
    //-- 
    $("a.running").each(function( index ) {

        var activityId = $(this).attr('id').replace('activity-timer-','');
        var input = $(this),
            time = input.html(),
            parts = time.split(':'),
            hours = +parts[0],
            minutes = +parts[1],
            seconds = +parts[2];

            timer = setInterval(function(){
                seconds++;
                if(seconds === 60) {
                    seconds = 00;
                    minutes++;

                    if(minutes === 60) {
                        minutes = 00;
                        hours++;
                    }
                }

                if (hours < 10 && String(hours).length < 2){
                    hours = '0' + hours;
                }

                if (minutes < 10 && String(minutes).length < 2){
                    minutes = '0' + minutes;
                }

                if (seconds < 10 && String(seconds).length < 2){
                    seconds = '0' + seconds;
                }

                var newTime = hours + ':' + minutes + ':' + seconds;
                var activityTimeId = '#activity-timer-' + activityId;
                $(activityTimeId).text(newTime);

            }, 1000);
        return;
    });

    /**
     * 
     */
    $('a.activity-timer').click(function (){

        addOverlay();

        var currentId = $(this).attr('id');
        var currentPath = $(this).attr('href');
        $.ajax({
             type: "POST",
             url: currentPath,
             data: { activity_id: currentId },
             error: function () {
                 alert('not working hiere');
                 
                 $('#overlay').remove();
             },
             success: function (msg){
                $(".alert").alert();
                if (msg === 'stop'){
                    
                    $('#' + currentId).removeClass('btn-warning');
                    $('#' + currentId).attr('href', currentPath.replace('stop','start'));
                    window.clearInterval(timer);
                }
                else {

                    $('.activity-timer').removeClass('btn-warning');

                    $('#' + currentId).addClass('btn-warning');
                    $('#' + currentId).attr('href', currentPath.replace('start','stop'));
                    
                    var input = $('#' + currentId);
                    var time = input.html();
                    
                    var parts = time.split(':'),
                    hours = +parts[0],
                    minutes = +parts[1],
                    seconds = +parts[2];
                    
                    window.clearInterval(timer);
                    timer = setInterval(function(){
                        seconds++;
                        if(seconds === 60) {
                            seconds = 00;
                            minutes++;

                            if(minutes === 60) {
                                minutes = 00;
                                hours++;
                            }
                        }

                        if (hours < 10 && String(hours).length < 2){
                            hours = '0' + hours;
                        }

                        if (minutes < 10 && String(minutes).length < 2){
                            minutes = '0' + minutes;
                        }

                        if (seconds < 10 && String(seconds).length < 2){
                            seconds = '0' + seconds;
                        }

                        var newTime = hours + ':' + minutes + ':' + seconds;
                        var activityTimeId = '#' + currentId;
                        $(activityTimeId).text(newTime);

                    }, 1000); 
                    newActivityTimeId = '2300';
                    var newrow = createTableRow (newActivityTimeId);
                    var id = currentId.replace('activity-timer-','');
                    $('#collapse' + id + ' .accordion-inner table.table').append(newrow);
                }
                $('#overlay').remove();
             }
         });

         return false;
    });


    $('.time-records').click(function () {
        $(this).parent('tbody').find('.time-records-table').slideToggle('slow');
    });

    $('.editable').editable(Routing.generate('activity_ajax_save') ,{
            indicator   : 'Saving...',
            method      : 'post',
            tooltip     : 'Click to edit...',
            style       : 'display: inline',
            height      : 22,
            submitdata  : function (value, settings, name){

            var currentClass = $(this).attr('class');
            var classArray = currentClass.split(' ');
            var activityIdClass = classArray[1];
            var activityId = activityIdClass.replace('activity-id-','');

            alert('hiers nu' + currentClass);
            return {id: activityId};
        },
        callback : function(value, settings) {
            $('.alert').alert();
            $(this).parent('p.lead').parent('div.accordion-toggle').parent('.accordion-heading').parent('div.accordion-group').addClass('success');
        }
    });

    $('.editable-description').editable(Routing.generate('activity_ajax_set_description'),{
            indicator : 'Saving...',
            type      : 'textarea',
            submit    : 'OK',
            cancel    : 'Cancel',
            method    : 'post',
            tooltip   : 'Click to edit...',
            style     : 'position: absolute',
            height    : 100,
            width     : 400,
            data      : function (value, settings) {
               /* alert(value);
                var reMarker = new reMarked();
                var markdown = reMarker.render(value);*/
        return value;
                //return markdown;
            },
            cssclass  : 'markdown',
            submitdata: function (value, settings, name){

                var currentClass = $(this).attr('class');
                var classArray = currentClass.split(' ');
                var activityIdClass = classArray[2];
                var activityId = activityIdClass.replace('activity-description-id-','');

                //alert('hiers nu' + name);
                return {id: activityId};
            },
        callback : function(value, settings) {
            $(".alert").alert();
            $(this).parent('p.lead').parent('div.accordion-toggle').parent('.accordion-heading').parent('div.accordion-group').addClass('success');
        }
    });



    $('.accordion-toggle .btn-group .btn.btn-actions').click(function (){
        window.location = $(this).attr('href');
        return false;
    });
    
    
    $('form.billable input').click(function () {
        
        addOverlay();
        $(this).parent('form').submit(function (e) {
            e.preventDefault();
        });

        var timeid = $(this).attr('id').replace('btn-billable-id-','');
        var timestatus = 1;
        if ($(this).hasClass('not-billable')){
            status = 0;
        }

        $.ajax({
             type: "POST",
             url: Routing.generate('activity_billable_ajax'),
             data: { id: timeid, status: timestatus },
             error: function () {
                 alert('not working hiere');
                 
                 $('#overlay').remove();
             },
             success: function (msg){
                alert(msg);
             }});
        
             $('#overlay').remove();
        alert('hiers');
    });
    //  billable-id-{{ timeRecord.id }}
    
    //$('a.btn.btn-show').colorbox({href: Routing.generate('activitytime_show', { id: btnid })});
  // $('a.btn.btn-show').click(function () {
  //      var btnid = $(this).attr('id');
  //      btnid = btnid.replace('btn-show-id-','');
  
  
  
        $('a.btn.btn-show, a.btn.btn-edit').colorbox({
            href:function (){
             
              btn = getCurrentTimeId(this);
              return Routing.generate(btn[1], { id: btn[0] });  
            },
            onOpen:function(){  
//$('#myModal').modal('show');
                btn = getCurrentTimeId(this);
                window.history.pushState("object or string", 'Edit Activity', Routing.generate(btn[1], {id: btn[0] })); 
            },
            onClosed:function(){ window.history.pushState("object or string", 'Activity', Routing.generate('activity')); }
        });
        

  //      return false;
  //  });
    
    //$.colorbox({href:"thankyou.html"});
});

function getCurrentTimeId(element) {
     var btnid = $(element).attr('id');
              var replaceString = 'btn-show-id-';
              var route = 'activitytime_show';
              if ($(element).hasClass('btn-edit') === true){

                  route = 'activitytime_edit';
                  replaceString = 'btn-edit-id-';
              }
             // btnid = btnid.replace(replaceString,'');
              
    return new Array(btnid.replace(replaceString,''), route);
}

function createTableRow (activityTimeId){
        var tableRow = '<tr>\n\
                        <td>\n\
                            0\n\
                        </td>\n\
                        <td>\n\
                            00-00-0000 00:00:00\n\
                        </td>\n\
                        <td>\n\
                            00-00-0000 00:00:00\n\
                        </td>\n\
                        <td>\n\
\n\
                          <div data-toggle="buttons-radio" class="btn-group pull-right">\n\
                              <a href="#" class="btn btn-success active">Billable</a>\n\
                              <a href="#" class="btn btn-danger">Not billable</a>\n\
                          </div>\n\
                         </td>\n\
                         <td>\n\
                          <div class="btn-group pull-right">\n\
                              <a id="btn-show-id-' + activityTimeId + '" href="' + Routing.generate('activitytime_show', { id: activityTimeId }) + '" class="btn btn-show">Show</a>\n\
                              <a href="' + Routing.generate('activitytime_edit', { id: activityTimeId }) + '" class="btn btn-edit"><i class="icon-edit hide-text">Edit</i></a>\n\
                              <a href="' + Routing.generate('activitytime_delete', { id: activityTimeId }) + '" class="btn btn-delete btn-danger">Delete</a>\n\
                          </div>\n\
                        </td>\n\
                    </tr>';
    return tableRow;
}