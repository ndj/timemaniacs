<?php
// src/Acme/DemoBundle/Menu/Builder.php
namespace Timemaniacs\ProjectBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $subMenu = $menu->addChild('Project');
        $subMenu->addChild('Projects', array('route' => 'project'));
        $subMenu->addChild('New Projects', array('route' => 'project_new'));

        return $menu;
    }
}
