<?php
// src/Acme/DemoBundle/Menu/Builder.php
namespace Timemaniacs\ProjectBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {

        $menu = $factory->createItem('activities2');

        $menu->addChild('Create new activity', array('route' => 'activity_new'));

        return $menu;
    }
    
    
    /**
     * 
     * @param \Knp\Menu\FactoryInterface $factory
     * @param array $options
     */
    public function getRecentActivitiesMenu(FactoryInterface $factory, array $options){
        $user = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->container->get('doctrine.orm.entity_manager');

        $entitiesActivities = $em->getRepository('TimemaniacsProjectBundle:Activity')->findBy(
            array( 'user' => $user),
            array( 'id' => 'DESC'),
            10
        );

        $menu = $factory->createItem('activities');

        foreach($entitiesActivities as $activity ){
            
           if ($activity->getActivityTimes()){
                $submenu = $menu->addChild($activity->getName());
                $submenu->setAttributes(array('class' => 'dropdown-submenu'));
                foreach($activity->getActivityTimes() as $time){
                    $submenu->addChild( $time->getId(),array('route' => 'activitytime_show', 'routeParameters' => array('id' => $time->getId())));
                }
            }
            else {
                $menu->addChild($activity->getName(), array('route' => 'activity_show', 'routeParameters' => array('id' => $activity->getId())));
            }
            
            if ($activity->getProject()){
                $submenu->addChild('View Kanban', array('route' => 'activity_show', 'routeParameters' => array('id' => $activity->getId())));
                $submenu->addChild('View gantt', array('route' => 'activity_show', 'routeParameters' => array('id' => $activity->getId())));
                $submenu->addChild('View git', array('route' => 'activity_show', 'routeParameters' => array('id' => $activity->getId())));
                $submenu->addChild('View mindmap', array('route' => 'activity_show', 'routeParameters' => array('id' => $activity->getId())));
            }
        }
        //$menu->addChild();
        
        return $menu;
    }
    
    
        /**
     * 
     * @param \Knp\Menu\FactoryInterface $factory
     * @param array $options
     */
    public function setRecentActivitiesMenu(FactoryInterface $factory, array $options){
        $user = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->container->get('doctrine.orm.entity_manager');

        $entitiesActivities = $em->getRepository('TimemaniacsProjectBundle:Activity')->findBy(
            array( 'user' => $user),
            array( 'id' => 'DESC'),
            10
        );

        $menu = $factory->createItem('activities');

        foreach($entitiesActivities as $activity ){
            
           $menu->addChild($activity->getName(), array('route' => 'activity_edit', 'routeParameters' => array('id' => $activity->getId())));
        }
        //$menu->addChild();
        
        return $menu;
    }
}
