<?php

namespace Timemaniacs\ProjectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Timemaniacs\ProjectBundle\Controller\Activity;

class KanbanController extends Controller
{
    /**
     * @Route("/kanban/{project}", name="kanban")
     * @Template()
     */
    public function indexAction($project)
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('TimemaniacsProjectBundle:Activity')->findBy(
            array(
                'project'   => $project,
            ),
            array(
                'running'   => 'DESC',
                'id'        => 'DESC'
            )
        );

        /**
         *
         */
        foreach ($entities as $counter => $entity){

            $service = $entity->getService();
            if (isset($service)) {

                $type = substr($service->getName(), 0, 3);
                $entity->queue .= strtoupper($type) . '_Q,S' . $counter . ',' . $entity->getName()  . PHP_EOL;
            }
            else {
                $entity->queue .= 'A_Q,S' . $counter . ',' . $entity->getName() . PHP_EOL;
            }
        }

        $serviceEntities = $em->getRepository('TimemaniacsServiceBundle:Service')->findBy(

            array(
                'kanban'   => 1,
            ),
            array(
                'kanbanWeight'   => 'ASC',
            )
        );

        return array(
            'kanban' => $entities,
            'serviceEntities' => $serviceEntities
        );
    }
}
