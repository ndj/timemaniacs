<?php

namespace Timemaniacs\ProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Timemaniacs\ProjectBundle\Entity\Story;
use Timemaniacs\ProjectBundle\Entity\Project;
use Timemaniacs\ProjectBundle\Form\StoryType;

/**
 * Story controller.
 *
 * @Route("/story")
 */
class StoryController extends Controller
{
    /**
     * Lists all Story entities.
     *
     * @Route("/", name="story")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TimemaniacsProjectBundle:Project')->findAll();
        
        return array(
            'entities' => $entities,
        );
    }
    
    
    /**
     * Lists all Story entities.
     *
     * @Route("/activities/{project}", name="story_actiities_project")
     * @Method("GET")
     * @Template()
     */
    public function indexProjectAction($project)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TimemaniacsProjectBundle:Story')->findBy(
            array( 'project'  => $project)
        );
        
        $activityEntities = $em->getRepository('TimemaniacsProjectBundle:Activity')->findBy(
            array( 
                'project'  => $project,
                'story' => NULL 
            )
        );

        return array(
            'entities' => $entities,
            'activityEntities' => $activityEntities
        );
    }

    /**
     * Creates a new Story entity.
     *
     * @Route("/", name="story_create")
     * @Method("POST")
     * @Template("TimemaniacsProjectBundle:Story:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Story();
        $form = $this->createForm(new StoryType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('story_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    
    /**
     * Creates a new Story entity.
     *
     * @Route("/create/{project}", name="story_create_for_project")
     * @Template("TimemaniacsProjectBundle:Story:new.html.twig")
     */
    public function createStoryForProjectAction(Request $request, $project)
    {
        $entity = new Story();
        $em = $this->getDoctrine()->getManager();

        $projectEntity = $em->getRepository('TimemaniacsProjectBundle:Project')->find($project);
        
       // var_dump($projectEntity);
        $entity->setProject($projectEntity);
        $form   = $this->createForm(new StoryType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }
    
    /**
     * Displays a form to create a new Story entity.
     *
     * @Route("/new", name="story_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Story();
        $form   = $this->createForm(new StoryType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Story entity.
     *
     * @Route("/{id}", name="story_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsProjectBundle:Story')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Story entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Story entity.
     *
     * @Route("/{id}/edit", name="story_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsProjectBundle:Story')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Story entity.');
        }

        $editForm = $this->createForm(new StoryType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Story entity.
     *
     * @Route("/{id}", name="story_update")
     * @Method("PUT")
     * @Template("TimemaniacsProjectBundle:Story:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsProjectBundle:Story')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Story entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new StoryType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('story_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Story entity.
     *
     * @Route("/{id}", name="story_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TimemaniacsProjectBundle:Story')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Story entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('story'));
    }

    /**
     * Creates a form to delete a Story entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
    
    
    
    
    /**
     * Ajax
     */
    /**
     * Displays a form to edit an existing Story entity.
     *
     * @Route("/ajax/edit", name="story_ajax_edit", options={"expose"=true})
     * @Method("POST")
     */
    public function ajaxEditAction(Request $request)
    {
       
       if ($request->isXmlHttpRequest()){
           
         $id = $request->request->get('id');
         $value = $request->request->get('value');

         $em = $this->getDoctrine()->getManager();
         $entity = $em->getRepository('TimemaniacsProjectBundle:Story')->find($id);
         
         if (!$entity) {
             throw $this->createNotFoundException('Unable to find Story entity.');
         }
         $entity->setName($value);

         $em->persist($entity);
         $em->flush();           

       }
       //$response = new Response(json_encode(array('out' => 'done', 'value' => $value)));
       //$response->headers->set('Content-Type', 'application/json');
       $response = new Response($value);
       return $response;
       
    }
    
    
   /**
     * @Route("/ajax/set/description", name="story_ajax_set_description", options={"expose"=true})
     * @Method("POST")
     */
    public function storyAjaxSetDescriptionAction(Request $request)
    {

        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $value = $request->request->get('value');
            $scriptStriptedValue = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);

            if ($value != $scriptStriptedValue){
               $scriptStriptedValue .= '<!-- script-tags removed -->';
            }

            $trimedValue = trim($scriptStriptedValue);
            $striptedValue = $trimedValue;

            $em = $this->getDoctrine()->getManager();
            $id = $request->request->get('id');

            $entity = $em->getRepository('TimemaniacsProjectBundle:Story')->find($id);
            $entity->setDescription($striptedValue);
            $em->persist($entity);
            $em->flush();
            echo $striptedValue;
            exit;
        }
        
        echo 'notting saved';
        exit;
    }
}
