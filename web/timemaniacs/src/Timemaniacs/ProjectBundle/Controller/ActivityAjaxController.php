<?php

namespace Timemaniacs\ProjectBundle\Controller;

use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Timemaniacs\TagBundle\Entity\Tag;
use Timemaniacs\ProjectBundle\Entity\Activity;
use Timemaniacs\ProjectBundle\Form\ActivityType;
use Timemaniacs\ProjectBundle\Entity\ActivityTime;

/**
 * ActivityTime controller.
 *
 * @Route("/activity")
 */
class ActivityAjaxController extends Controller
{
  
    /**
     * @Route("/ajax/set/description", name="activity_ajax_set_description", options={"expose"=true})
     * @Method("POST")
     */
    public function activityAjaxSetDescriptionAction(Request $request)
    {

        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $value = $request->request->get('value');
            $scriptStriptedValue = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);

            if ($value != $scriptStriptedValue){
               $scriptStriptedValue .= '<!-- script-tags removed -->';
            }

            $trimedValue = trim($scriptStriptedValue);
            $striptedValue = $trimedValue;

            $em = $this->getDoctrine()->getManager();
            $id = $request->request->get('id');

            $entity = $em->getRepository('TimemaniacsProjectBundle:Activity')->find($id);
            $entity->setDescription($striptedValue);
            $em->persist($entity);
            $em->flush();
            echo $striptedValue;
            exit;
        }
        
        echo 'notting saved';
        exit;
    }

    /**
     * @Route("/ajax/get/description", name="activity_ajax_get_description")
     * @Method("POST")
     */
    public function activityAjaxGetDescriptionAction(Request $request)
    {

        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $id = $request->query->get('id');

            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TimemaniacsProjectBundle:Activity')->find($id);
            echo $entity->getDescription();
            exit;
        }
        
        echo 'notting saved';
        exit;
    }
    
    
        /**
     * Edits an existing Activity entity.
     *
     * @Route("/{id}/stop", name="activity_stop")
     * @Method("POST")
     */
    public function stopActivityAction(Request $request, $id)
    {
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $em = $this->getDoctrine()->getManager();
            $runningActivity = $em->getRepository('TimemaniacsProjectBundle:Activity')->findOneBy(array('id' => $id, 'running' => 1));

            if ($runningActivity){

                $runningActivity->setRunning(0);
                foreach ($runningActivity->getActivityTimes() as $timeRecord ){

                    $emptyEndedTimeRecord = $timeRecord->getEnd();
                    if (empty($emptyEndedTimeRecord)){

                        $now = new \DateTime('NOW');
                        $timeRecord->setEnd($now);
                        $totalTime = abs($now->format('U') - $timeRecord->getStart()->format('U'));
                        $timeRecord->setTotalTime($totalTime);
                    }   
                }
                $runningActivity->setRunning(0);
                $em->persist($runningActivity);
                $em->flush();

                echo 'stop';
                exit;
            }

            echo 'no record';
            exit;
        }

        echo 'no ajax';
        exit;
    }

    /**
     * 
     *
     * @Route("/ajax/save", name="activity_ajax_save", options={"expose"=true})
     * @Method("POST")
     */
    public function activityAjaxSaveAction(Request $request)
    {
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $value = $request->request->get('value');
            $scriptStriptedValue = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $value);

            if ($value != $scriptStriptedValue){
               $scriptStriptedValue .= '<!-- script-tags removed -->';
            }

            $trimedValue = trim($scriptStriptedValue);
            $striptedValue = $trimedValue;

            $em = $this->getDoctrine()->getManager();
            $id = $request->request->get('id');
            $entity = $em->getRepository('TimemaniacsProjectBundle:Activity')->find($id);
            $entity->setName($striptedValue);
            $em->persist($entity);
            $em->flush();
            echo $striptedValue;
            exit;
        }
        
        echo 'notting saved';
        exit;
    }
    
        /**
     * Edits an existing Activity entity.
     *
     * @Route("/{id}/start", name="activity_start")
     * @Method("POST")
     */
    public function startActivityAction(Request $request, $id)
    {
        // ajax check
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $user = $this->getUser();
            //$companies = '';
            $em = $this->getDoctrine()->getManager();
            $runningActivities = $em->getRepository('TimemaniacsProjectBundle:Activity')->findBy(array('user' => $user, 'running' => 1));
            $now = new \DateTime('NOW');

            foreach ($runningActivities as $runningActivity) {

                $runningActivity->setRunning(0);
                $runningActivity->setEnd($now);
                $activityTimes = $em->getRepository('TimemaniacsProjectBundle:ActivityTime')->findBy(array('activity' => $runningActivity->getId()));
                foreach ($activityTimes as $activityTime ) {
                    $activityTime->setEnd($now);
                    $em->persist($activityTime);
                }
                $em->persist($runningActivity);
            }

            $newActivityTime = new ActivityTime();
            $newActivity = $em->getRepository('TimemaniacsProjectBundle:Activity')->find($id);
            $newActivityTime->setActivity($newActivity);
            $newActivityTime->setStart($now);
            $newActivity->setRunning(1);
            $newActivity->setStart($now);

            $em->persist($newActivity);
            $em->persist($newActivityTime);
            $em->flush();

            echo 'start';
            exit;
        }

        echo 'no ajax';
        exit;
    }
    
    
        /**
     * Edits an existing Activity entity.
     *
     * @Route("/billable/ajax/status", name="activity_billable_ajax", options={"expose"=true})
     * @Method("POST")
     * 
     */
    public function updateBillableAction(Request $request)
    {
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $em = $this->getDoctrine()->getManager();
            $id = $request->request->get('id');
            $status = $request->request->get('status');
            
            $entity = $em->getRepository('TimemaniacsProjectBundle:ActivityTime')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ActivityTime entity.');
            }


            $editForm = $this->createForm(new ActivityTimeType(), $entity);
            $editForm->bind($request);

           // if ($editForm->isValid()) {

                $entity->setBillable($status);
                $em->persist($entity);
                $em->flush();

                //return $this->redirect($this->generateUrl('activity', array('id' => $id)));

           // }
            echo 'done';
            exit;
        }

 
    }
    
    
}
