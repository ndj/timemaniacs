<?php

namespace Timemaniacs\ProjectBundle\Controller;

use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Timemaniacs\TagBundle\Entity\Tag;
use Timemaniacs\ProjectBundle\Entity\Activity;
use Timemaniacs\ProjectBundle\Form\ActivityType;
use Timemaniacs\ProjectBundle\Entity\ActivityTime;

/**
 * Activity controller.
 *
 * @Route("/activity")
 */
class ActivityController extends Controller
{
    /**
     * Lists all Activity entities.
     *
     * @Route("/", name="activity", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('TimemaniacsProjectBundle:Activity')->findBy(
            array(),
            array(
                'running'   => 'DESC',
                'start'     => 'DESC',
                'id'        => 'DESC',
            )
        );

        foreach ($entities as $entity){

            $time = 0;
            foreach($entity->getActivityTimes() as $timeRecord ){
                $end = $timeRecord->getEnd();
                $start = $timeRecord->getStart();
                if (isset($end) && isset($start)){
                   $time += $timeRecord->getEnd()->format('U') - $timeRecord->getStart()->format('U');
                }
            }

            $totalTime = $entity->getTotalTime();
            if (empty($totalTime) && $time == 0){
                $today = new \DateTime('NOW');
                $todayTimestamp = $today->format('U');
                //Refactor::getStart
                $createdTimestamp = $entity->getStart()->format('U');
                $totalTime = $todayTimestamp - $createdTimestamp;
            }

            $compleetTime = $totalTime + $time;

            if ($compleetTime < 0){
                $compleetTime = 0;
            }

            $entity->newTotalTime = $this->secondsToHMS($compleetTime);
        }

        $serviceEntities = $em->getRepository('TimemaniacsServiceBundle:Service')->findBy(
            array(
                'kanban'   => 1,
            ),
            array(
                'kanbanWeight'   => 'ASC',
            )
        );

        return array(
            'entities' => $entities,
            'serviceEntities' => $serviceEntities
        );
    }

    /**
     * Creates a new Activity entity.
     *
     * @Route("/", name="activity_create")
     * @Method("POST")
     * @Template("TimemaniacsProjectBundle:Activity:new.html.twig")
     */
    public function createAction(Request $request)
    {
        
        $entity  = new Activity();
        $form = $this->createForm(new ActivityType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            $user = $this->getUser();
            //$companies = '';
            $em = $this->getDoctrine()->getManager();

            // Stop the current running task form your comany ofcourse 
            $runningActivities = $em->getRepository('TimemaniacsProjectBundle:Activity')->findBy(array('user' => $user, 'running' => 1));

            foreach ($runningActivities as $runningActivity) {
                $runningActivity->setRunning(0);
                $runningActivity->setEnd(new \DateTime('NOW'));
                $em->persist($runningActivity);
            }

            $entity->setRunning(1);
            $entity->setUser($user);
            $entity->setStart(new \DateTime('NOW'));
            $activityTimeEntity = new ActivityTime();
            $activityTimeEntity->setTotalTime(0);
            $activityTimeEntity->setStart(new \DateTime('NOW'));
            $activityTimeEntity->setActivity($entity);

            $em->persist($activityTimeEntity);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('activity', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Activity entity.
     *
     * @Route("/new", name="activity_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Activity();
        $form   = $this->createForm(new ActivityType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Activity entity.
     *
     * @Route("/{id}", name="activity_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsProjectBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Activity entity.
     *
     * @Route("/{id}/edit", name="activity_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsProjectBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        //$totalTime = $entity->getTotaltimetoArray($entity->getTotaltime());
        $editForm = $this->createForm(new ActivityType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Activity entity.
     *
     * @Route("/{id}", name="activity_update")
     * @Method("PUT")
     * @Template("TimemaniacsProjectBundle:Activity:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('TimemaniacsProjectBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ActivityType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {

            $diffTime = $entity->getEnd()->format('U') - $entity->getStart()->format('U'); 

            if ($diffTime == 0){
                $now = new \DateTime('NOW');
                $diffTime = $now->format('U') - $entity->getStart()->format('U');
                $entity->setEnd($now);
            }

            $entity->setTotalTime($diffTime);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('activity', array('id' => $id)));
            
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }



    /**
     * Deletes a Activity entity.
     *
     * @Route("/{id}", name="activity_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TimemaniacsProjectBundle:Activity')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Activity entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('activity'));
    }

    /**
     * Creates a form to delete a Activity entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
    /**
     * Edits an existing Activity entity.
     *
     * @Route("/{id}/billable/{status}", name="activity_billable")
     * @Method("POST")
     * @Template("TimemaniacsProjectBundle:Activity:index.html.twig")
     */
    public function updateBillableAction(Request $request, $id, $status)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('TimemaniacsProjectBundle:ActivityTime')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ActivityTime entity.');
        }


        $editForm = $this->createForm(new ActivityTimeType(), $entity);
        $editForm->bind($request);

       // if ($editForm->isValid()) {

            $entity->setBillable($status);
            $em->persist($entity);
            $em->flush();

            //return $this->redirect($this->generateUrl('activity', array('id' => $id)));
            
       // }

        return $this->redirect($this->generateUrl('activity', array('id' => $id)));
    }
    
    
    
    
  /**
   * 
   * @param type $seconds
   * @return type
   */
  function secondsToHMS($seconds)
  {

     $days = floor($seconds/86400);
     $hrs = floor($seconds/3600);
     $mins = intval(($seconds / 60) % 60); 
     $sec = intval($seconds % 60);

        if($days>0){
          //echo $days;exit;
          $hrs = str_pad($hrs,2,'0',STR_PAD_LEFT);
          $hours=$hrs-($days*24);
          $return_days = $days." Days ";
          $hrs = str_pad($hours,2,'0',STR_PAD_LEFT);
     }else{
      $return_days="";
      $hrs = str_pad($hrs,2,'0',STR_PAD_LEFT);
     }

     $mins = str_pad($mins,2,'0',STR_PAD_LEFT);
     $sec = str_pad($sec,2,'0',STR_PAD_LEFT);

     return $return_days.$hrs.":".$mins.":".$sec;
  }

  
    /**
     * Lists all Story entities.
     *
     * @Route("/project/{project}", name="activities_project")
     * @Method("GET")
     * @Template()
     */
    public function indexProjectAction($project)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TimemaniacsProjectBundle:Project')->findBy(
            array( 'id' => $project )
        );

        return array(
            'entities' => $entities,
        );
    }
  
  
}
