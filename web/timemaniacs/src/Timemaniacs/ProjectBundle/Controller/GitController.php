<?php

namespace Timemaniacs\ProjectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Gitonomy\Git\Repository;

/**
 * Project controller.
 *
 * @Route("/git")
 */
class GitController extends Controller {
    //put your code here
    
     /**
     * @Route("/branches", name="git")
     * @Template()
     */
    public function indexAction(){

        $repository = new Repository('../../../');
         $out = '';
        foreach ($repository->getReferences()->getBranches() as $branch) {
            $out .= "- ".$branch->getName();
        }

        return array('out' => $out);
        //var_dump($repository->run('fetch', array('--all')));
    }
}

?>
