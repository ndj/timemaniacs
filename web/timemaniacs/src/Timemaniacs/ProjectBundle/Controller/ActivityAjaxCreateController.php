<?php

namespace Timemaniacs\ProjectBundle\Controller;

use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Timemaniacs\TagBundle\Entity\Tag;
use Timemaniacs\ProjectBundle\Entity\Project;
use Timemaniacs\ProjectBundle\Entity\Activity;
use Timemaniacs\ProjectBundle\Form\ActivityType;
use Timemaniacs\ProjectBundle\Entity\ActivityTime;

/**
 * ActivityTime controller.
 *
 * @Route("/activity")
 */
class ActivityAjaxCreateController extends Controller {

    /**
     * Creates a new Activity entity.
     *
     * All formats
     *   '- 2h' ||  '- 22h' -- last 2 hours or 22 h  
     *   '+ 2h' ||  '+ 22h' -- for the comming 2 hours or 22 h
     *   
     * 
     * @Route("/ajax", name="activity_create_ajax", options={"expose"=true})
     * @Method("POST")
     * @Template("TimemaniacsProjectBundle:Activity:new.html.twig")
     */
    public function createAjaxAction(Request $request)
    {
        $entity  = new Activity();
        $form = $this->createForm(new ActivityType(), $entity);
        $form->bind($request);
        $matchesHours = array();
        $matchesDate = array();
        $matchesHoursformat2 = array();
        $activity_array = array();
        $matchesProject = array();
        if ($form->isValid()) {

            $name = trim($request->request->get('name'));

            $splitChar = '|';

            $time = FALSE;
            $startTime = FALSE;
            $date= FALSE;
            $project= FALSE;

            // here we gone validate the time
            if (strstr($name,$splitChar)) {

                $addTime = FALSE;
                if ($name[0] == '+'){
                    $addTime = TRUE;
                    $name = substr($name, 1);
                }
                else if ($name[0] == '-') {
                    $name = substr($name, 1);
                }

                $activity_array = explode($splitChar,$name);

                // matches 2h or 22h 
                $hours = '/[0-2]{0,1}[0-9 ]h/';
                $hoursformat2 = '/([0-2][0-3]:[0-5][0-9])|(0?[0-9]:[0-5][0-9])/';

                // matches 01-03-2013 or 10/20/2010
                // valid for all months except for feb. is 29 day possible 
                // format MM/DD/YYYY
                $pregDate = '/((((0[13578])|(1[02]))[\/,-]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\/,-]?(([0-2][0-9])|(30)))|(02[\/,-]?[0-2][0-9]))[\/,-]?\d{4}/';
                $pregProject = '/@(\S+)\s?/';
                
                preg_match($hours,trim($activity_array[0]), $matchesHours);
                preg_match($hoursformat2,trim($activity_array[0]), $matchesHoursformat2);
                preg_match($pregDate,trim($activity_array[0]), $matchesDate);
                preg_match($pregProject,trim($activity_array[0]), $matchesProject);
                
                
                

                /**
                 * 
                 * $stopInjectionVerbs = "/(alter|begin|cast|convert|create|cursor|declare|delete|drop|end|exec|fetch|insert|kill|open|select|sys|table|update)/";
$errors = array();

if (preg_match($stopInjectionVerbs, $select)) {
$errors[] = "Can not use SQL injection Strings";
}
                 * 
                 */

                if (isset($matchesHours[0])){
                    $time = str_replace('h', '', $matchesHours[0]);
                }
                
                if (isset($matchesHoursformat2[0])){
                    $addTime = TRUE;
                    $startTime = $matchesHoursformat2[0];
                    
                }

                if (isset($matchesDate[0])){
                    $addTime = TRUE;
                    $date = $matchesDate[0];
                }
                
                if (isset($matchesProject[0])) {
                    $project = $matchesProject[0];
                }

                $entity->setRunning(0);
                $entity->setName(trim($activity_array[1]));
            }
            else {
                $entity->setName($name);
                $entity->setRunning(1);
            }

            $user = $this->getUser();
            $entity->setUser($user);
            $entity->setStart(new \DateTime('NOW'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            if ($time) {
                $newActivityTime = new ActivityTime();

                $activityDate = 'NOW';
                if ($date || isset($matchesHoursformat2[0])){
                    $activityDate = $date . ' ' . date('h:i:s');

                    if ($date && isset($matchesHoursformat2[0])){
                        $activityDate = $date . ' ' . $startTime . ':00';
                    }
                    else if (isset($matchesHoursformat2[0])){
                        $activityDate = date('d:m:Y') . ' ' . $startTime . ':00';
                    }
                }

                if (!$addTime){
                    $newActivityTime->setActivity($entity);
                    $startDate = new DateTime($activityDate);
                    $startDate->setTimestamp($startDate->format('U') - (60 *60) * $time);
                    $newActivityTime->setStart($startDate);
                    $endDate = new DateTime($activityDate);
                    $newActivityTime->setEnd($endDate);
                }
                else {
                    $newActivityTime->setActivity($entity);
                    $startDate = new DateTime($activityDate);
                    $newActivityTime->setStart($startDate);
                    $endDate = new DateTime($activityDate);
                    $endDate->setTimestamp($startDate->format('U') + (60 *60) * $time);
                    $newActivityTime->setEnd($endDate);
                }

                if ($project){
                    
                    $projectName = str_replace('@', '', $project);
                    $projectEntity = $em->getRepository('TimemaniacsProjectBundle:Project')->findOneBy(
                        array( 'name' => $projectName)
                    );
                    
                    if ($projectEntity){
                        $entity->setProject($projectEntity);
                    }

                }
                
                
                $em->persist($newActivityTime);
                $em->flush();

                $return = json_encode(array('id' =>$entity->getId(), 'name' => trim($activity_array[1]), 'start' => $startDate->format('d-m-Y h:i:s'), 'end' => $endDate->format('d-m-Y h:i:s'), 'project' => $project));
            }
            else {
               $return = json_encode(array('id' =>$entity->getId(), 'name' => $name));
            }

            echo $return;
            exit;
        }
        echo 'Error';
        exit;
    }
}
