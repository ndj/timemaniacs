<?php

namespace Timemaniacs\ProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Timemaniacs\ProjectBundle\Entity\Mindmap;
use Timemaniacs\ProjectBundle\Form\MindmapType;

/**
 * Mindmap controller.
 *
 * @Route("/mindmap")
 */
class MindmapController extends Controller
{
    /**
     * Lists all Mindmap entities.
     *
     * @Route("/", name="mindmap")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TimemaniacsProjectBundle:Mindmap')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Mindmap entity.
     *
     * @Route("/", name="mindmap_create")
     * @Method("POST")
     * @Template("TimemaniacsProjectBundle:Mindmap:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Mindmap();
        $form = $this->createForm(new MindmapType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('mindmap_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Mindmap entity.
     *
     * @Route("/new", name="mindmap_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Mindmap();
        $form   = $this->createForm(new MindmapType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Mindmap entity.
     *
     * @Route("/show/{id}", name="mindmap_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsProjectBundle:Mindmap')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mindmap entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $string = unserialize($entity->getMap());
        return array(
            'tmp' => $string,
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Mindmap entity.
     *
     * @Route("/{id}/edit", name="mindmap_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsProjectBundle:Mindmap')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mindmap entity.');
        }

        $editForm = $this->createForm(new MindmapType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Mindmap entity.
     *
     * @Route("/update/{id}", name="mindmap_update")
     * @Method("PUT")
     * @Template("TimemaniacsProjectBundle:Mindmap:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsProjectBundle:Mindmap')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mindmap entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new MindmapType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('mindmap_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Mindmap entity.
     *
     * @Route("/{id}", name="mindmap_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TimemaniacsProjectBundle:Mindmap')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Mindmap entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('mindmap'));
    }

    /**
     * Creates a form to delete a Mindmap entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
    
    
    
    
    
    
    
       /**
     * Lists all Project entities.
     *
     * @Route("/tmp/tmp", name="mindmaptmptmp")
     * @Method("GET")
     * @Template("TimemaniacsProjectBundle:Mindmap:show.html.twig")
     */
    public function showtmAction(){
        
         $em = $this->getDoctrine()->getManager();
         $entities = $em->getRepository('TimemaniacsProjectBundle:Mindmap')->findBy(
            array(),
            array(
                'id'        => 'DESC',
            )
        );
         
        $string = unserialize($entities[0]->getMap());
         
        //$string = '{"title":"1 shosssunnld check all","id":1, "attr": { "style": { "background": "#FF0000" }, "attachment": { "contentType": "text/html", "content": "content <b>bold content</b>"}}, "ideas":{"1":{"attr": {"collapsed": true }, "title":"2 is very very lng www.google.com","id":2,"ideas":{"1":{"title":"3 is also very long","id":3}}},"11":{"title":"4","id":4,"ideas":{"1":{"title":"5","id":5,"ideas":{"1":{"title":"6","id":6},"2":{"title":"7 is long","id":7,"ideas":{"1":{"title":"8","id":8},"2":{"title":"9","id":9},"3":{"title":"10","id":10},"4":{"title":"11","id":11},"5":{"title":"12","id":12}}}}}}},"12":{"title":"A cunning plan...","id":15},"13":{"title":"A cunning plan...","id":17},"14":{"title":"Well be famous...","id":19},"15":{"title":"A brilliant idea...","id":21,"ideas":{"1":{"title":"A brilliant idea...","id":24},"2":{"title":"A cunning plan...","id":25},"3":{"title":"A brilliant idea...","id":26},"4":{"title":"A brilliant idea...","id":27},"5":{"title":"A brilliant idea...","id":28},"6":{"title":"A cunning plan...","id":29},"7":{"title":"A brilliant idea...","id":30},"8":{"title":"A brilliant idea...","id":31}}},"16":{"title":"A cunning plan...","id":23},"-1":{"title":"A brilliant idea...","id":13},"-2":{"title":"A brilliant idea...","id":14},"-3":{"title":"A cunning plan...","id":16},"-4":{"title":"Well be famous...","id":18},"-5":{"title":"Well be famous...","id":20},"-6":{"title":"A brilliant idea...","id":22}}}';
        //$string = unserialize($string);
        return array('tmp' => $string);
    }
    
    
    /**
     * @Route("/get/json", name="mindmap_get_json")
     * @Method("POST")
     */
    public function getMindmupJsonAction(Request $request){
        
      //  $value = '{"title":"1 shosssunnld check all","id":1, "attr": { "style": { "background": "#FF0000" }, "attachment": { "contentType": "text/html", "content": "content <b>bold content</b>"}}, "ideas":{"1":{"attr": {"collapsed": true }, "title":"2 is very very lng www.google.com","id":2,"ideas":{"1":{"title":"3 is also very long","id":3}}},"11":{"title":"4","id":4,"ideas":{"1":{"title":"5","id":5,"ideas":{"1":{"title":"6","id":6},"2":{"title":"7 is long","id":7,"ideas":{"1":{"title":"8","id":8},"2":{"title":"9","id":9},"3":{"title":"10","id":10},"4":{"title":"11","id":11},"5":{"title":"12","id":12}}}}}}},"12":{"title":"A cunning plan...","id":15},"13":{"title":"A cunning plan...","id":17},"14":{"title":"Well be famous...","id":19},"15":{"title":"A brilliant idea...","id":21,"ideas":{"1":{"title":"A brilliant idea...","id":24},"2":{"title":"A cunning plan...","id":25},"3":{"title":"A brilliant idea...","id":26},"4":{"title":"A brilliant idea...","id":27},"5":{"title":"A brilliant idea...","id":28},"6":{"title":"A cunning plan...","id":29},"7":{"title":"A brilliant idea...","id":30},"8":{"title":"A brilliant idea...","id":31}}},"16":{"title":"A cunning plan...","id":23},"-1":{"title":"A brilliant idea...","id":13},"-2":{"title":"A brilliant idea...","id":14},"-3":{"title":"A cunning plan...","id":16},"-4":{"title":"Well be famous...","id":18},"-5":{"title":"Well be famous...","id":20},"-6":{"title":"A brilliant idea...","id":22}}}';
        
      //  echo $value;
      //  exit;
        
        $value = $request->request->get('value');
        
        
        echo 'done';
        exit;
        
        
        
        
        $response = new Response($value);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
        //exit;
        
        $jsonArray = json_decode($value);
        $response = new Response(json_encode($jsonArray));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
        
    }
    
    
    /**
     * @Route("/set/json/{id}", name="mindmap_set_json")
     * @Method("POST")
     */
    public function setMindmupJsonAction(Request $request, $id){
        
        $value = $request->request->get('name');
        
        //$entity  = new MindMap();
        $em = $this->getDoctrine()->getManager();
        //$string = unserialize($entities[0]->getMap());
        $entity = $em->getRepository('TimemaniacsProjectBundle:Mindmap')->findOneBy(
            array('id' => $id)
        );
        
        $entity->setName('testmap');
        $entity->setMap(serialize($value));
        $entity->setCompany(12);
        $entity->setCreated(new \DateTime());
        $entity->setUpdated(new \DateTime());
        $user = $this->getUser();
        $entity->setUser($user);
        
        
        $em->persist($entity);
        $em->flush();
        
        
        
        echo $value;
        exit;
    }
    
}
