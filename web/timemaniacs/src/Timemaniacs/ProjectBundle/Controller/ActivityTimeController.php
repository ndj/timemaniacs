<?php

namespace Timemaniacs\ProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Timemaniacs\ProjectBundle\Entity\ActivityTime;
use Timemaniacs\ProjectBundle\Form\ActivityTimeType;

/**
 * ActivityTime controller.
 *
 * @Route("/activitytime")
 */
class ActivityTimeController extends Controller
{
    /**
     * Lists all ActivityTime entities.
     *
     * @Route("/", name="activitytime")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TimemaniacsProjectBundle:ActivityTime')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new ActivityTime entity.
     *
     * @Route("/", name="activitytime_create")
     * @Method("POST")
     * @Template("TimemaniacsProjectBundle:ActivityTime:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new ActivityTime();
        $form = $this->createForm(new ActivityTimeType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('activitytime_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new ActivityTime entity.
     *
     * @Route("/new", name="activitytime_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new ActivityTime();
        $form   = $this->createForm(new ActivityTimeType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a ActivityTime entity.
     *
     * @Route("/{id}", name="activitytime_show", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsProjectBundle:ActivityTime')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ActivityTime entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ActivityTime entity.
     *
     * @Route("/{id}/edit", name="activitytime_edit", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsProjectBundle:ActivityTime')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ActivityTime entity.');
        }

        $editForm = $this->createForm(new ActivityTimeType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing ActivityTime entity.
     *
     * @Route("/{id}", name="activitytime_update")
     * @Method("PUT")
     * @Template("TimemaniacsProjectBundle:ActivityTime:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsProjectBundle:ActivityTime')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ActivityTime entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ActivityTimeType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('activitytime_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a ActivityTime entity.
     *
     * @Route("/{id}", name="activitytime_delete", options={"expose"=true})
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TimemaniacsProjectBundle:ActivityTime')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ActivityTime entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('activitytime'));
    }

    /**
     * Creates a form to delete a ActivityTime entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
