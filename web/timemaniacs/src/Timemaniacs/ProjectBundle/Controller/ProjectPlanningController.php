<?php

namespace Timemaniacs\ProjectBundle\Controller;

use stdClass;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Timemaniacs\ProjectBundle\Entity\Story;
use Timemaniacs\ProjectBundle\Entity\Project;
use Timemaniacs\ProjectBundle\Entity\Activity;
use Timemaniacs\ProjectBundle\Entity\ActivityTime;

use Timemaniacs\ProjectBundle\Form\ProjectType;

/**
 * Project controller.
 *
 * @Route("/projectplanning")
 */
class ProjectPlanningController extends Controller
{
    /**
     * Lists all Project entities.
     *
     * @Route("/{project}", name="projectplanning")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($project)
    {
        $em = $this->getDoctrine()->getManager();

        $projects = $em->getRepository('TimemaniacsProjectBundle:Project')->findBy(
            array(
                'id' => $project,
            )
        );

        $activities = array();
        $counter = 0;
        foreach($projects as $project) {
            $counter++;
            $activity = new stdClass();
            $activity->id = $counter;
            $activity->name = $project->getName();
            $activity->code = 'project' .$project->getId();
            $activity->level = 0;
            $activity->status = 'STATUS_ACTIVE';
            $dateStart = $project->getStart();
            $dateEnd = $project->getDeadline();

            $timestampStart = $dateStart->format('U');
            $timestampEnd = $dateEnd->format('U');

            $activity->duration = ceil(abs($timestampEnd - $timestampStart) / (60*60*24));
            $activity->start = $timestampStart *1000;
            $activity->end = $timestampEnd*1000;
            $activity->startIsMileStone = true;
            $activity->enIsMileStoen = false;
            $activity->assigs = '';

            $activities[] = $activity;
            $parentId = $counter;

            foreach($project->getActivities() as $projectActivity){
                $counter++;
                $activity = new stdClass();
                $activity->id = $counter;
                $activity->name = $projectActivity->getName();
                $service = $projectActivity->getService();
                $activity->code = $projectActivity->getId() ;
                $activity->level = 1;
                $activity->status = 'STATUS_DONE';
                $dateStart = $projectActivity->getStart();
                $timestamp = $dateStart->format('U') *1000;
                $activity->start = $timestamp ;
                $end = $projectActivity->getEnd();
                $dateEnd = empty($end) ? new DateTime('NOW') :  $projectActivity->getEnd();
                $activity->end = $dateEnd->format('U') *1000;
                $activity->duration = $projectActivity->getEstimatedDays();//ceil(abs($dateEnd->format('U') - $dateStart->format('U')) / (60*60*24));;

                $activity->startIsMileStone = false;
                //TMP TMP TMP !!!!!test
              /*  if ($counter > 4){
                    $activity->depends = '4';
                }*/
                $parent = $projectActivity->getParent();
                if (!empty($parent)){
                    // $activity->level = 2;
                   // var_dump($parent->getId());
                    $activity->depends = (String)$parent->getId();
                }
                
                $activity->enIsMileStoen = false;
                $activity->assigs = '';

                $activities[] = $activity;
            }
        }

        $tasks = new stdClass();
        $tasks->tasks = $activities;
        $tasks->selectedRow = 0;
        $tasks->deletedTaskIds = array();
        $tasks->canWrite = true;
        $tasks->canWriteOnParent = true;

        return array(
            'tasks' => $tasks,
            'project' => $projects[0]->getId()
        );
    }
    
    /**
     * Lists all Project entities.
     *
     * @Route("/ajax", name="set_projectplanning_ajax")
     * @Method("POST")
     */
    public function setProjectPlanningAjaxAction(Request $request){

        $data = $request->request->get('data');
        $projectId = $request->request->get('project');

        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository('TimemaniacsProjectBundle:Project')->findOneBy(
            array(
                'id' => $projectId,
            )
        );
        
        foreach($data['tasks'] as $counter => $activity){

            if (empty($activity['code'])){

                $entity  = new Activity();
                $entity->setProject($project);
                $entity->setName($activity['name']);

                $dateStart = new DateTime();
                $dateStart->setTimestamp($activity['start']/1000);
                $entity->setStart($dateStart);

                $dateEnd = new DateTime();
                $dateEnd->setTimestamp($activity['end']/1000);
                $entity->setEnd($dateEnd);
                $entity->setEstimatedDays($activity['duration']);
                $activityTimeEntity = new ActivityTime();
                $activityTimeEntity->setTotalTime(0);
                $activityTimeEntity->setActivity($entity);

               // $activityTimeEntity->setChilderen();
                
                $em->persist($activityTimeEntity);
                $em->persist($entity);
                
                $em->flush();
                
                echo $entity->getId();
 
            }
            else {
      
                if ($activity['level'] != 0){
                    
                    $getActivity = $em->getRepository('TimemaniacsProjectBundle:Activity')->findOneBy(
                            
                        array(
                            'id' => $activity['code'],
                        )
                    );
                    
                    if ($getActivity){
                        $getActivity->setName($activity['name']);

                        $dateStart = new DateTime();
                        $dateStart->setTimestamp($activity['start']/1000);
                        $getActivity->setStart($dateStart);

                        $dateEnd = new DateTime();
                        $dateEnd->setTimestamp($activity['end']/1000);
                        $getActivity->setEnd($dateEnd);
                        
                        $duration = $activity['duration'];
      
                        if (isset($activity['depends'])){

                            $getParent = $em->getRepository('TimemaniacsProjectBundle:Activity')->findOneBy(
                                array(
                                    'id' => $activity['depends'],
                                )
                            );

                           $id = $getActivity->getId();
                           $getActivity->setParent($getParent);

                        }
                    }

                    $em->persist($getActivity);
                    $em->flush();
                 }
            }
            
        }
        
       // $em->flush();
       
        echo 'done';
        exit;
        
    }

    
    /**
     * Lists all Project entities.
     *
     * @Route("/story/ajax", name="set_projectplanning_story_ajax")
     * @Method("POST")
     */
    public function setProjectStoryPlanningAjaxAction(Request $request){

        
        
        $data = $request->request->get('data');
        $projectId = $request->request->get('project');

        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository('TimemaniacsProjectBundle:Project')->findOneBy(
            array(
                'id' => $projectId,
            )
        );
        
        
        foreach($data['tasks'] as $counter => $activity){

            if (empty($activity['code'])){

                $entity  = new Story();
                $entity->setProject($project);
                $entity->setName($activity['name']);

                $dateStart = new DateTime();
                $dateStart->setTimestamp($activity['start']/1000);
                $entity->setStart($dateStart);

                $dateEnd = new DateTime();
                $dateEnd->setTimestamp($activity['end']/1000);
                $entity->setEnd($dateEnd);
                //$entity->setEstimatedDays($activity['duration']);
                
                /*$activityTimeEntity = new ActivityTime();
                $activityTimeEntity->setTotalTime(0);
                $activityTimeEntity->setActivity($entity);
*/
  
                $entity->setActivities(NULL);
                $entity->setDescription('click to edit');
                $entity->setProject($project);
                $entity->setCompany($project->getCompany());
                $entity->setSprint(1);
                $entity->setDeadline(new DateTime());

                
               // $activityTimeEntity->setChilderen();
               
               // $em->persist($activityTimeEntity);
                $em->persist($entity);
               // echo 'dezez' . $project->getId() ; exit;
                $em->flush();
                
                //echo $entity->getId();
                 
 
            }
            else {
      
                if ($activity['level'] != 0){
                    
                    $getActivity = $em->getRepository('TimemaniacsProjectBundle:Activity')->findOneBy(
                            
                        array(
                            'id' => $activity['code'],
                        )
                    );
                    
                    if ($getActivity){
                        $getActivity->setName($activity['name']);

                        $dateStart = new DateTime();
                        $dateStart->setTimestamp($activity['start']/1000);
                        $getActivity->setStart($dateStart);

                        $dateEnd = new DateTime();
                        $dateEnd->setTimestamp($activity['end']/1000);
                        $getActivity->setEnd($dateEnd);
                        
                        $duration = $activity['duration'];
      
                        if (isset($activity['depends'])){

                            $getParent = $em->getRepository('TimemaniacsProjectBundle:Activity')->findOneBy(
                                array(
                                    'id' => $activity['depends'],
                                )
                            );

                           $id = $getActivity->getId();
                           $getActivity->setParent($getParent);

                        }
                    }

                    $em->persist($getActivity);
                    $em->flush();
                 }
            }
            
        }
        
       // $em->flush();
       
        echo 'done';
        exit;
        
    }
    
    
    

    /**
     * Lists all Project entities.
     *
     * @Route("/story/planning/{project}", name="projectplanning_story")
     * @Method("GET")
     * @Template("TimemaniacsProjectBundle:ProjectPlanning:index.html.twig")
     */
    public function indexProjectStoryAction($project)
    {
        
        $em = $this->getDoctrine()->getManager();

        $projects = $em->getRepository('TimemaniacsProjectBundle:Project')->findBy(
            array(
                'id' => $project,
            )
        );
//var_dump($projects);
        $activities = array();
        $counter = 0;
        foreach($projects as $project) {
            $counter++;
            $activity = new stdClass();
            $activity->id = $counter;
            $activity->name = $project->getName();
            $activity->code = $project->getId();
            $activity->level = 0;
            $activity->status = 'STATUS_ACTIVE';
            $dateStart = $project->getStart();
            $dateEnd = $project->getDeadline();

            $timestampStart = $dateStart->format('U');
            $timestampEnd = $dateEnd->format('U');

            $activity->duration = ceil(abs($timestampEnd - $timestampStart) / (60*60*24));
            $activity->start = $timestampStart *1000;
            $activity->end = $timestampEnd*1000;
            $activity->startIsMileStone = true;
            $activity->enIsMileStoen = false;
            $activity->assigs = '';

            $activities[] = $activity;
            $parentId = $counter;

            foreach ($project->getStories() as $projectStories) {
                
                $counter++;
                $activity = new stdClass();
                $activity->id = $counter;
                $activity->name = $projectStories->getName();
                $service = 1;
                $activity->code = $projectStories->getId() ;
                $activity->level = 1;
                $activity->status = 'STATUS_DONE';
                $dateStart = $projectStories->getStart();
                $timestamp = $dateStart->format('U') *1000;
                $activity->start = $timestamp ;
              //  $end = $projectActivity->getEnd();
               /// $dateEnd = (empty(end)) ? new DateTime('NOW') :  $projectActivity->getEnd();
               // $activity->end = $dateEnd->format('U') *1000;
                $activity->duration = ceil(abs($dateEnd->format('U') - $dateStart->format('U')) / (60*60*24));;

                $activity->startIsMileStone = false;
                $activities[] = $activity;
                
                foreach($projectStories->getActivities() as $projectActivity){
                    $counter++;
                    $activity = new stdClass();
                    $activity->id = $counter;
                    $activity->name = $projectActivity->getName();
                    $service = $projectActivity->getService();
                    $activity->code = $projectActivity->getId() ;
                    $activity->level = 2;
                    $activity->status = 'STATUS_DONE';
                    $dateStart = $projectActivity->getStart();
                    $timestamp = $dateStart->format('U') *1000;
                    $activity->start = $timestamp ;
                  //  $end = $projectActivity->getEnd();
                   /// $dateEnd = (empty(end)) ? new DateTime('NOW') :  $projectActivity->getEnd();
                   // $activity->end = $dateEnd->format('U') *1000;
                    $activity->duration = ceil(abs($dateEnd->format('U') - $dateStart->format('U')) / (60*60*24));;

                    $activity->startIsMileStone = false;
                    //TMP TMP TMP !!!!!test
                   // if ($counter > 4){
                    $parent = $projectActivity->getParent();
                    if (isset($parent)){

                      //  var_dump($parent->getId());

                        $activity->depends = (String)$parent->getId();
                    }
                   // }

                    $activity->enIsMileStoen = false;
                    $activity->assigs = '';
                    $activities[] = $activity;

                }
                $activities[] = $activity;
            }
        }

        $tasks = new stdClass();
        $tasks->tasks = $activities;
        $tasks->selectedRow = 0;
        $tasks->deletedTaskIds = array();
        $tasks->canWrite = true;
        $tasks->canWriteOnParent = true;

        return array(
            'tasks' => $tasks,
            'project' => $projects[0]->getId()
        );
    }
    
    
    
}

?>
