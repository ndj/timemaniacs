<?php

namespace Timemaniacs\NodeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Timemaniacs\NodeBundle\Entity\Wiki;
use Timemaniacs\NodeBundle\Form\WikiType;

/**
 * Wiki controller.
 *
 * @Route("/wiki")
 */
class WikiController extends Controller
{
    /**
     * Lists all Wiki entities.
     *
     * @Route("/", name="wiki")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TimemaniacsNodeBundle:Wiki')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    
    /**
     * Lists all Wiki entities.
     *
     * @Route("/project/{project}", name="wiki_project")
     * @Method("GET")
     * @Template("TimemaniacsNodeBundle:Wiki:index.html.twig")
     */
    public function indexProjectAction($project)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TimemaniacsNodeBundle:Wiki')->findBy(
            array(
                'project' => $project 
            )
        );

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Wiki entity.
     *
     * @Route("/", name="wiki_create")
     * @Method("POST")
     * @Template("TimemaniacsNodeBundle:Wiki:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Wiki();
        $form = $this->createForm(new WikiType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('wiki_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Wiki entity.
     *
     * @Route("/new", name="wiki_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Wiki();
        $form   = $this->createForm(new WikiType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Wiki entity.
     *
     * @Route("/{id}", name="wiki_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsNodeBundle:Wiki')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Wiki entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Wiki entity.
     *
     * @Route("/{id}/edit", name="wiki_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsNodeBundle:Wiki')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Wiki entity.');
        }

        $editForm = $this->createForm(new WikiType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Wiki entity.
     *
     * @Route("/{id}", name="wiki_update")
     * @Method("PUT")
     * @Template("TimemaniacsNodeBundle:Wiki:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TimemaniacsNodeBundle:Wiki')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Wiki entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new WikiType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('wiki_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Wiki entity.
     *
     * @Route("/{id}", name="wiki_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TimemaniacsNodeBundle:Wiki')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Wiki entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('wiki'));
    }

    /**
     * Creates a form to delete a Wiki entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
