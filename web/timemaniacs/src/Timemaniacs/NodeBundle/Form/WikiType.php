<?php

namespace Timemaniacs\NodeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WikiType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('uri')
            ->add('alias')
            ->add('project')
            ->add('menuTitle')
            ->add('title')
            ->add('body')
            
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Timemaniacs\NodeBundle\Entity\Wiki'
        ));
    }

    public function getName()
    {
        return 'timemaniacs_nodebundle_wikitype';
    }
}
