<?php

namespace Timemaniacs\NodeBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Wiki
 *
 * @ORM\Table(name="wiki")
 * @ORM\Entity(repositoryClass="Timemaniacs\NodeBundle\Entity\WikiRepository")
 */
class Wiki extends Node
{
    /**
     * @var string
     *
     * @ORM\Column(name="menu_title", type="string", length=255)
     */
    private $menuTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="uri", type="string", length=255)
     */
    private $uri;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="alias", type="object")
     */
    private $alias;

    /**
     * @ORM\ManyToOne(targetEntity="\Timemaniacs\ProjectBundle\Entity\Project", inversedBy="wikimedia")
     * @ORM\JoinColumn(nullable=true)
     */
    private $project;


    /**
     * Set menuTitle
     *
     * @param string $menuTitle
     * @return Wiki
     */
    public function setMenuTitle($menuTitle)
    {
        $this->menuTitle = $menuTitle;
    
        return $this;
    }

    /**
     * Get menuTitle
     *
     * @return string 
     */
    public function getMenuTitle()
    {
        return $this->menuTitle;
    }

    /**
     * Set uri
     *
     * @param string $uri
     * @return Wiki
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    
        return $this;
    }

    /**
     * Get uri
     *
     * @return string 
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set alias
     *
     * @param \stdClass $alias
     * @return Wiki
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    
        return $this;
    }

    /**
     * Get alias
     *
     * @return \stdClass 
     */
    public function getAlias()
    {
        return $this->alias;
    }
    
    public function getProject() {
        return $this->project;
    }

    public function setProject($project) {
        $this->project = $project;
    }


}
