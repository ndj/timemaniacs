<?php

namespace Timemaniacs\ServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Service
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Timemaniacs\ServiceBundle\Entity\ServiceRepository")
 */
class Service
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="rate", type="integer")
     */
    private $rate;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     *
     * @ORM\OneToMany(targetEntity="\Timemaniacs\ProjectBundle\Entity\Activity", mappedBy="service")
     */
    private $activities;
    
    /**
     *
     * @ORM\OneToMany(targetEntity="\Timemaniacs\ProjectBundle\Entity\Kanban\Kanban", mappedBy="service")
     */
    private $kanbanActivities;

    /**
     * @var boolean
     *
     * @ORM\Column(name="kanban", type="boolean")
     */
    private $kanban;
     /**
     *
     * @var type
     * @ORM\Column(name="kanbanWeight", type="integer") 
     */
    private $kanbanWeight;   
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    public function __construct() {
        $this->activities = new ArrayCollection();
        $this->kanbanActivities = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set rate
     *
     * @param integer $rate
     * @return Service
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    
        return $this;
    }

    /**
     * Get rate
     *
     * @return integer 
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Service
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function getActivities() {
        return $this->activities;
    }

    public function setActivity($activities) {
        $this->activities = $activities;
    }

    public function getKanban() {
        return $this->kanban;
    }

    public function setKanban($kanban) {
        $this->kanban = $kanban;
    }
    public function getKanbanWeight() {
        return $this->kanbanWeight;
    }

    public function setKanbanWeight($kabanWeight) {
        $this->kanbanWeight = $kabanWeight;
    }
    public function getKanbanActivities() {
        return $this->kanbanActivities;
    }

    public function setKanbanActivities($kanbanActivities) {
        $this->kanbanActivities = $kanbanActivities;
    }

                
    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Service
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Service
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    
        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * 
     * @return type
     */
    public function __toString() {
        return $this->name;
    }
}
