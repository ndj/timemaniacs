<?php
// src/Acme/DemoBundle/Menu/Builder.php
namespace Timemaniacs\ServiceBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $subMenu = $menu->addChild('Service');
        $subMenu->addChild('Services', array('route' => 'project'));
        $subMenu->addChild('New Service', array('route' => 'project_new'));

        return $menu;
    }
}
