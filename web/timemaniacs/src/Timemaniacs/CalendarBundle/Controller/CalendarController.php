<?php

namespace Timemaniacs\CalendarBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\SecurityExtraBundle\Annotation\Secure;

class CalendarController extends Controller
{
    /**
     * @Route("/calendar", name="calendar")
     * @Template()
     */
    public function indexAction()
    {
        return array('name' => 'helolo');
    }
}
