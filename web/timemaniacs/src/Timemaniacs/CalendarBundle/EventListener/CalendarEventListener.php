<?php

namespace Timemaniacs\CalendarBundle\EventListener;

use ADesigns\CalendarBundle\Event\CalendarEvent;
use ADesigns\CalendarBundle\Entity\EventEntity;
use Doctrine\ORM\EntityManager;

class CalendarEventListener
{
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function loadEvents(CalendarEvent $calendarEvent)
    {
        $startDate = $calendarEvent->getStartDatetime();
        $endDate = $calendarEvent->getEndDatetime();

        $activities = $this->entityManager->getRepository('TimemaniacsProjectBundle:Activity')
                          ->createQueryBuilder('AT')
                          ->where('AT.created BETWEEN :startDate and :endDate')
                          ->setParameter('startDate', $startDate->format('Y-m-d H:i:s'))
                          ->setParameter('endDate', $endDate->format('Y-m-d H:i:s'))
                          ->getQuery()->getResult();

        foreach($activities as $activity) {

            $activityEntity = new EventEntity($activity->getName(), $activity->getStart(), $activity->getStart());

            //optional calendar event settings
            $activityEntity->setAllDay(true); // default is false, set to true if this is an all day event
            $activityEntity->setBgColor('#f00'); //set the background color of the event's label
            $activityEntity->setFgColor('#fff'); //set the foreground color of the event's label

            $activityEntity->setUrl('/activity/'.$activity->getId()); // $this->generateUrl('activity_show', array('id' => $companyEvent->getId()))
            $activityEntity->setCssClass('my-custom-class'); // a custom class you may want to apply to event labels

            //finally, add the event to the CalendarEvent for displaying on the calendar
            $calendarEvent->addEvent($activityEntity);
        }
    }
}
