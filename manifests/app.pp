exec {
 'apt-get-update':
    path    => '/usr/bin',
    command => 'apt-get update'
}
package {
 'htop':
    ensure  => present,
    require => Exec['apt-get-update']
}
package{
 'git-core':
    ensure  => present
}
package{
 'vim':
    ensure  => present
}
package{
 'curl':
    ensure  => present
}
package{
 'yui-compressor':
    ensure  => present,
    require => Exec['apt-get-update']
}
package{
 'acl':
    ensure  => present
}
class {
 'apache': 
    require => Exec['apt-get-update'],
}
package{
 'openjdk-7-jre-headless':
    ensure  => present,
    require => Exec['apt-get-update']
}
apache::module {'rewrite': }
apache::module {'env': }
apache::module {'deflate': }
apache::module {'expires': }

class {
 'mysql': 
    root_password => root,
        require   => Exec['apt-get-update'],
}
class { 
 'php':     
    require => Exec['apt-get-update']
}

php::module { 'cli': }
php::module { 'curl': }
php::module { 'mcrypt': }
php::module { 'intl': }
php::module { 'sqlite': }
php::module { 'xdebug': }
php::module { 'ldap': }
php::module {
 'apc': 
  module_prefix => 'php-'
}
php::module {
 'pear':
  module_prefix => 'php-'
}

package{
 'sendmail':
    ensure  => present,
    require => Exec['apt-get-update']
}
package{
 'phpmyadmin':
    ensure  => present,
    require => [Class['php'], Class['mysql']]
}

exec {
 'pear-auto-discover':
    path    => '/usr/bin:/usr/sbin:/bin',
    onlyif  => 'test "`pear config-get auto_discover`" = "0"',
    command => 'pear config-set auto_discover 1 system',
    require => Php::Module['cli']
}
exec {
 'pear-update':
    path    => '/usr/bin:/usr/sbin:/bin',
    command => 'pear update-channels && pear upgrade-all',
    require => Php::Module['cli']
}
exec {
 'install-phpunit':
    unless  => '/usr/bin/which phpunit',
    command => '/usr/bin/pear install pear.phpunit.de/PHPUnit --alldeps',
    require => [Php::Module['cli'], Exec['pear-auto-discover'], Exec['pear-update']]
}
exec {
 'install-phpqatools':
    unless  => '/usr/bin/which phpcs',
    command => '/usr/bin/pear install pear.phpqatools.org/phpqatools --alldeps',
    require => [Php::Module['cli'], Exec['pear-auto-discover'], Exec['pear-update']]
}
exec {
 'install-phpdocumentor':
    unless  => '/usr/bin/which phpdoc',
    command => '/usr/bin/pear install pear.phpdoc.org/phpDocumentor-alpha --alldeps',
    require => [Php::Module['cli'], Exec['pear-auto-discover'], Exec['pear-update']]
}
exec {
 'create-dir':
    path    => '/usr/bin:/usr/sbin:/bin',
    command => 'mkdir -p /home/vagrant/code/web',
    unless  => "[ -d '/home/vagrant/code/web' ]"
}
exec {
 'download-composer':
    path    => '/usr/bin:/usr/sbin:/bin',
    command => 'curl -s https://getcomposer.org/installer | php -- --install-dir=/home/vagrant/code/web/timemaniacs',
    require => [Package['curl'],Php::Module['cli'],Exec['create-dir'],File['php_cliphpini']],
}
file {
 'php_apachephpini':
    path    => '/etc/php5/apache2/php.ini',
    ensure  => present,
    source  => '/home/vagrant/code/resources/php.ini',
    require => Php::Module['cli'],
}
file {
 'php_cliphpini':
    path    => '/etc/php5/cli/php.ini',
    ensure  => '/etc/php5/apache2/php.ini',
    require => File['php_apachephpini'],
}
file {
 'php_mcrypt':
    path    => '/etc/php5/conf.d/mcrypt.ini',
    ensure  => '/home/vagrant/code/resources/mcrypt.ini',
    require => File['php_apachephpini'],
}

file {
 'apache2.default-vhost':
    path    => '/etc/apache2/sites-available/default',
    ensure  => present,
    source  => '/home/vagrant/code/resources/apache2-default-vhost',
    require => Php::Module['cli'],
}
file {
 '/home/vagrant/code/web/adminer':
    ensure  => 'directory',
}
exec{
 'download_adminer':
    path    => '/usr/bin:/usr/sbin:/bin',
    cwd     => '/home/vagrant/code/web/adminer/',
    command => 'wget http://www.adminer.org/latest-mysql-en.php -O adminer.php',
    require => File['/home/vagrant/code/web/adminer'],
}
file {
 'apache2.adminer-vhost':
    path    => '/etc/apache2/sites-available/adminer',
    ensure  => present,
    source  => '/home/vagrant/code/resources/apache2-adminer-vhost',
    require => Exec['create-dir'],
}
file {
 'apache2.timemaniacs-vhost':
    path    => '/etc/apache2/sites-available/timemaniacs',
    ensure  => present,
    source  => '/home/vagrant/code/resources/apache2-timemaniacs-vhost',
    require => Exec['create-dir'],
}
host { 
 'adminer':
    ip           => '127.0.0.1',
    host_aliases => 'adminer.dev',
}
host { 
 'timemaniacs':
    ip           => '127.0.0.1',
    host_aliases => 'timemaniacs.dev',
}
host { 
 'gitonomy':
    ip           => '127.0.0.1',
    host_aliases => 'gitonomy.dev',
}
file {
 'apache2.gitonomy-vhost':
    path    => '/etc/apache2/sites-available/gitonomy',
    ensure  => present,
    source  => '/home/vagrant/code/resources/apache2-gitonomy-vhost',
    require => Exec['create-dir'],
}
exec{
 'apache2_en_site_adminer':
    path    => '/usr/bin:/usr/sbin:/bin',
    command => 'a2ensite adminer',
    require => File['apache2.adminer-vhost']
}
exec{
 'apache2_en_site_timemaniacs':
    path    => '/usr/bin:/usr/sbin:/bin',
    command => 'a2ensite timemaniacs',
    require => File['apache2.timemaniacs-vhost']
}
exec{
 'restart_apache2':
    path    => '/usr/bin:/usr/sbin:/bin',
    command => 'service apache2 restart',
    require => Exec['apache2_en_site_timemaniacs']
}
exec{
 'update_composer_install_vendors':
    path    => '/usr/bin:/usr/sbin:/bin',
    cwd     => '/home/vagrant/code/web/timemaniacs', 
    command => 'php composer.phar update',
    require => [File['php_apachephpini'],File['php_cliphpini'],Exec['download-composer'],Exec['restart_apache2']]
}

exec{
 'console_create_database':
    path    => '/usr/bin:/usr/sbin:/bin',
    unless  =>  'mysql -uroot -proot timemaniacs',
    cwd     => '/home/vagrant/code/web/timemaniacs', 
    command => 'php app/console doctrine:database:create',
    require => Exec['update_composer_install_vendors']
}
exec{
 'console_update_schema':
    path    => '/usr/bin:/usr/sbin:/bin',
    cwd     => '/home/vagrant/code/web/timemaniacs', 
    command => 'php app/console doctrine:schema:update --force',
    require => Exec['console_create_database']
}
 exec { 
  'clone_mindmup':
    command => 'git clone git://github.com/mindmup/mapjs.git mindmup',
    path    => '/usr/bin:/usr/sbin:/bin',
    creates => '/home/vagrant/code/web/timemaniacs/web/js/mindmup',
    cwd     => '/home/vagrant/code/web/timemaniacs/web/js',
    timeout => '0',
    require => Exec['console_create_database']
  }
exec{
 'get_mindmap_dependencies':
    path    => '/usr/bin:/usr/sbin:/bin',
    cwd     => '/home/vagrant/code/web/timemaniacs/web/js/mindmup', 
    command => 'bash dl_dependencies.sh',
    require => Exec['clone_mindmup']
}
exec{
 'get_mindmap_dependencies_compile':
    path    => '/usr/bin:/usr/sbin:/bin',
    cwd     => '/home/vagrant/code/web/timemaniacs/web/js/mindmup', 
    command => 'bash compile.sh',
    require => Exec['get_mindmap_dependencies']
}
exec { 
  'clone_gantt':
    command => 'git clone git://github.com/robicch/jQueryGantt.git gantt',
    path    => '/usr/bin:/usr/sbin:/bin',
    creates => '/home/vagrant/code/web/timemaniacs/web/js/gantt',
    cwd     => '/home/vagrant/code/web/timemaniacs/web/js',
    timeout => '0',
    require => Exec['console_create_database']
}
cron  {
 'set_cron_mysql_backup':
    command => '/usr/bin/mysqldump -uroot -proot timemaniacs > /home/vagrant/code/web/timemaniacs.sql',
    user    => root,
    #weekday => 3,
    minute => '*/30',
    #minute  => $hostname ? {
    #                   server-vm1     => "15,45",
    #                   server-vm2     => "0,30"
    #           },
    ##hour    => ['8-20'];
}
exec { 
  'clone_gitonomy':
    command => 'git clone git://github.com/gitonomy/gitonomy.git gitonomy',
    path    => '/usr/bin:/usr/sbin:/bin',
    creates => '/home/vagrant/code/web/gitonomy',
    cwd     => '/home/vagrant/code/web',
    timeout => '0',
    require => Exec['get_mindmap_dependencies']
}
exec{
 'apache2_en_site_gitonomy':
    path    => '/usr/bin:/usr/sbin:/bin',
    command => 'a2ensite gitonomy',
    require => Exec['clone_gitonomy']
}
cron  {
 'set_cron_ssh_gitonomy':
    command => '/usr/bin/mysqldump -uroot -proot timemaniacs > /home/vagrant/code/web/timemaniacs.sql',
    user    => root,
    #weekday => 3,
    minute => '*/30',
    #minute  => $hostname ? {
    #                   server-vm1     => "15,45",
    #                   server-vm2     => "0,30"
    #           },
    ##hour    => ['8-20'];
}
cron  {
 'set_cron_gitonomy':
    command => '/usr/bin/php /home/vagrant/code/web/gitonomy/app/console authorized:keys -i > ~/.ssh/authorized_keys',
    user    => root,
    #weekday => 3,
    #minute => '*',
    #minute  => $hostname ? {
    #                   server-vm1     => "15,45",
    #                   server-vm2     => "0,30"
    #           },
    ##hour    => ['8-20'];
}
#exec{
# 'gitonomy_remove_parameters_file':
#    path    => '/usr/bin:/usr/sbin:/bin',
#    cwd     => '/home/vagrant/code/web/gitonomy',
#    command => 'rm app/config/parameters.yml',
#    require => Exec['clone_gitonomy']
#}
exec{
 'console_update_gems':
    path    => '/usr/bin:/usr/sbin:/bin:/opt/vagrant_ruby/bin',
    command => 'gem update --system',
        require => Exec['apache2_en_site_gitonomy']
}
exec{
 'console_install_composer':
    path    => '/usr/bin:/usr/sbin:/bin:/opt/vagrant_ruby/bin',
    command => 'gem install compass',
        require => Exec['console_update_gems']
}
file { '/usr/local/bin/compass':
    ensure => 'link',
    target => '/opt/vagrant_ruby/bin/compass',
 require => Exec['console_install_composer']
}
file {
 'gitonomy-parameters-file':
    path    => '/home/vagrant/code/web/gitonomy/app/config/parameters.yml',
    ensure  => present,
    source  => '/home/vagrant/code/resources/parameters-gitonomy',
    #require => Exec['gitonomy_remove_parameters_file'],
    require => File['/usr/local/bin/compass']
}
exec {
 'download-composer-gitonomy':
    path    => '/usr/bin:/usr/sbin:/bin',
    command => 'curl -s https://getcomposer.org/installer | php -- --install-dir=/home/vagrant/code/web/gitonomy',
    require => [Package['curl'],Php::Module['cli'],Exec['create-dir'],File['/usr/local/bin/compass']],
}
exec{
 'console_create_database_gitonomy':
    path    => '/usr/bin:/usr/sbin:/bin',
    cwd     => '/home/vagrant/code/web/gitonomy', 
    command => 'php composer.phar install --optimize-autoloader --prefer-dist',
        require => Exec['download-composer-gitonomy']
}
exec{
 'console_create_database_gitonomy_command':
    path    => '/usr/bin:/usr/sbin:/bin',
    unless  =>  'mysql -uroot -proot gitonomy',
    cwd     => '/home/vagrant/code/web/gitonomy', 
    command => 'php app/console doctrine:database:create',
        require => Exec['console_create_database_gitonomy']
}
exec{
 'gitonomy_install':
    path    => '/usr/bin:/usr/sbin:/bin',
    unless  =>  'mysql -u root -e "use gitonomy;desc project;" -proot',
    cwd     => '/home/vagrant/code/web/gitonomy',
    command => 'bash install.sh',
    require => Exec['console_create_database_gitonomy_command']
}
exec{
 'restart_apache2_gitonomy':
    path    => '/usr/bin:/usr/sbin:/bin',
    command => 'service apache2 restart',
    require => Exec['apache2_en_site_gitonomy']
}

exec{
 'download_solr':
    path    => '/usr/bin:/usr/sbin:/bin',
    cwd     => '/tmp',
    creates  =>  "/tmp/solr-4.2.1.tgz",
    command => 'wget http://apache.cu.be/lucene/solr/4.2.1/solr-4.2.1.tgz -O solr-4.2.1.tgz',
    require => Exec['restart_apache2_gitonomy'],
}
file {
 '/home/vagrant/servers':
    ensure  => 'directory',
    require => Exec['download_solr'],
}
exec { 'install_solr':
  path    => '/usr/bin:/usr/sbin:/bin',
  cwd => '/tmp',
  command => 'tar xvzf solr-4.2.1.tgz  -C /home/vagrant/servers',
  creates => '/home/vagrant/server/solr',
  require => File['/home/vagrant/servers'],
}
