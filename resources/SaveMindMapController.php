<?php

namespace Timemaniacs\ProjectBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Timemaniacs\ProjectBundle\Entity\Mindmap;

/**
 * Project controller.
 *
 * @Route("/old/mindmap")
 */
class SaveMindMapController extends Controller {

    /**
     * Lists all Project entities.
     *
     * @Route("/tmp", name="mindmapsss")
     * Method("GET")
     * @Template()
     */
    public function indexAction(){
        
         $em = $this->getDoctrine()->getManager();
         $entities = $em->getRepository('TimemaniacsProjectBundle:Mindmap')->findBy(
            array(),
            array(
                'id'        => 'DESC',
            )
        );
         
        $string = unserialize($entities[0]->getMap());
         
        //$string = '{"title":"1 shosssunnld check all","id":1, "attr": { "style": { "background": "#FF0000" }, "attachment": { "contentType": "text/html", "content": "content <b>bold content</b>"}}, "ideas":{"1":{"attr": {"collapsed": true }, "title":"2 is very very lng www.google.com","id":2,"ideas":{"1":{"title":"3 is also very long","id":3}}},"11":{"title":"4","id":4,"ideas":{"1":{"title":"5","id":5,"ideas":{"1":{"title":"6","id":6},"2":{"title":"7 is long","id":7,"ideas":{"1":{"title":"8","id":8},"2":{"title":"9","id":9},"3":{"title":"10","id":10},"4":{"title":"11","id":11},"5":{"title":"12","id":12}}}}}}},"12":{"title":"A cunning plan...","id":15},"13":{"title":"A cunning plan...","id":17},"14":{"title":"Well be famous...","id":19},"15":{"title":"A brilliant idea...","id":21,"ideas":{"1":{"title":"A brilliant idea...","id":24},"2":{"title":"A cunning plan...","id":25},"3":{"title":"A brilliant idea...","id":26},"4":{"title":"A brilliant idea...","id":27},"5":{"title":"A brilliant idea...","id":28},"6":{"title":"A cunning plan...","id":29},"7":{"title":"A brilliant idea...","id":30},"8":{"title":"A brilliant idea...","id":31}}},"16":{"title":"A cunning plan...","id":23},"-1":{"title":"A brilliant idea...","id":13},"-2":{"title":"A brilliant idea...","id":14},"-3":{"title":"A cunning plan...","id":16},"-4":{"title":"Well be famous...","id":18},"-5":{"title":"Well be famous...","id":20},"-6":{"title":"A brilliant idea...","id":22}}}';
        //$string = unserialize($string);
        return array('tmp' => $string);
    }
    
    
    /**
     * @Route("/get/json", name="mindmap_ggggget_json")
     * @Method("POST")
     */
    public function getMindmupJsonAction(Request $request){
        
      //  $value = '{"title":"1 shosssunnld check all","id":1, "attr": { "style": { "background": "#FF0000" }, "attachment": { "contentType": "text/html", "content": "content <b>bold content</b>"}}, "ideas":{"1":{"attr": {"collapsed": true }, "title":"2 is very very lng www.google.com","id":2,"ideas":{"1":{"title":"3 is also very long","id":3}}},"11":{"title":"4","id":4,"ideas":{"1":{"title":"5","id":5,"ideas":{"1":{"title":"6","id":6},"2":{"title":"7 is long","id":7,"ideas":{"1":{"title":"8","id":8},"2":{"title":"9","id":9},"3":{"title":"10","id":10},"4":{"title":"11","id":11},"5":{"title":"12","id":12}}}}}}},"12":{"title":"A cunning plan...","id":15},"13":{"title":"A cunning plan...","id":17},"14":{"title":"Well be famous...","id":19},"15":{"title":"A brilliant idea...","id":21,"ideas":{"1":{"title":"A brilliant idea...","id":24},"2":{"title":"A cunning plan...","id":25},"3":{"title":"A brilliant idea...","id":26},"4":{"title":"A brilliant idea...","id":27},"5":{"title":"A brilliant idea...","id":28},"6":{"title":"A cunning plan...","id":29},"7":{"title":"A brilliant idea...","id":30},"8":{"title":"A brilliant idea...","id":31}}},"16":{"title":"A cunning plan...","id":23},"-1":{"title":"A brilliant idea...","id":13},"-2":{"title":"A brilliant idea...","id":14},"-3":{"title":"A cunning plan...","id":16},"-4":{"title":"Well be famous...","id":18},"-5":{"title":"Well be famous...","id":20},"-6":{"title":"A brilliant idea...","id":22}}}';
        
      //  echo $value;
      //  exit;
        
        $value = $request->request->get('value');
        
        
        echo 'done';
        exit;
        
        
        
        
        $response = new Response($value);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
        //exit;
        
        $jsonArray = json_decode($value);
        $response = new Response(json_encode($jsonArray));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
        
    }
    
    
    /**
     * @Route("/set/json", name="mindmap_sssssssssset_json")
     * @Method("POST")
     */
    public function setMindmupJsonAction(Request $request){
        
        $value = $request->request->get('name');
        
        $entity  = new MindMap();
        $entity->setName('testmap');
        $entity->setMap(serialize($value));
        $entity->setCompany(12);
        $entity->setCreated(new \DateTime());
        $entity->setUpdated(new \DateTime());
        $user = $this->getUser();
        $entity->setUser($user);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        
        
        
        echo $value;
        exit;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
   
    public function createAction(Request $request)
    {
        
        $entity  = new Activity();
        $form = $this->createForm(new ActivityType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {

            $user = $this->getUser();
            //$companies = '';
            $em = $this->getDoctrine()->getManager();

            // Stop the current running task form your comany ofcourse 
            $runningActivities = $em->getRepository('TimemaniacsProjectBundle:Activity')->findBy(array('user' => $user, 'running' => 1));

            foreach ($runningActivities as $runningActivity) {
                $runningActivity->setRunning(0);
                $runningActivity->setEnd(new \DateTime('NOW'));
                $em->persist($runningActivity);
            }

            $entity->setRunning(1);
            $entity->setUser($user);
            $entity->setStart(new \DateTime('NOW'));
            $activityTimeEntity = new ActivityTime();
            $activityTimeEntity->setTotalTime(0);
            $activityTimeEntity->setStart(new \DateTime('NOW'));
            $activityTimeEntity->setActivity($entity);

            $em->persist($activityTimeEntity);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('activity', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }
    
    
    
    
    
    
    
}

?>
